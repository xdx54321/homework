# 演示视频

链接: https://pan.baidu.com/s/1BvfcDHvInvPIzAG1PuezDg  密码: mjmn

# 环境搭建过程

## Kafka集群搭建

基于 docker & docker-compose 来搭建

### 镜像选择
zookeeper官方镜像，版本3.4
kafka采用wurstmeister/kafka镜像

### 集群规划
| 主机名 |   IP地址    | 端口  | 
|-------| ----------  |------|
|  zoo1 | 172.19.0.11 |2184:2181|
|  zoo2 | 172.19.0.12 |2185:2181|
|  zoo3 | 172.19.0.13 |2186:2181|
| kafka1| 172.19.0.14 |9092:9092|
| kafka2| 172.19.0.15 |9093:9093|
| kafka3| 172.19.0.16 |9094:9094|

### 宿主机创建目录
```
mkdir -p ~/zkcluster/zoo1/data
mkdir -p ~/zkcluster/zoo2/data
mkdir -p ~/zkcluster/zoo3/data

mkdir -p ~/zkcluster/zoo1/datalog
mkdir -p ~/zkcluster/zoo2/datalog
mkdir -p ~/zkcluster/zoo3/datalog

mkdir -p kafka1/logs
mkdir -p kafka2/logs
mkdir -p kafka3/logs

# 宿主机修改hosts
172.16.0.14 kafka1
172.16.0.15 kafka2
172.16.0.16 kafka3
```
### zookeeper集群的docker-compose.yml
```
version: '3.1'

services:
  zoo1:
    image: zookeeper:3.5
    restart: always
    hostname: zoo1
    container_name: zoo1
    ports:
      - 2185:2181
    volumes:
     - "/home/xdx/zkcluster/zoo1/data:/data"
     - "/home/xdx/zkcluster/zoo1/datalog:/datalog"
    networks:
      br17216:
        ipv4_address: 172.16.0.11

  zoo2:
    image: zookeeper:3.5
    restart: always
    hostname: zoo2
    container_name: zoo2
    ports:
      - 2186:2181
    volumes:
     - "/home/xdx/zkcluster/zoo2/data:/data"
     - "/home/xdx/zkcluster/zoo2/datalog:/datalog"
    networks:
      br17216:
        ipv4_address: 172.16.0.12

  zoo3:
    image: zookeeper:3.5
    restart: always
    hostname: zoo3
    container_name: zoo3
    ports:
      - 2187:2181
    volumes:
     - "/home/xdx/zkcluster/zoo3/data:/data"
     - "/home/xdx/zkcluster/zoo3/datalog:/datalog"
    networks:
      br17216:
        ipv4_address: 172.16.0.13

  web:
    image: elkozmon/zoonavigator-web:0.5.0
    container_name: zoonavigator-web
    ports:
     - "8000:8000"
    environment:
      WEB_HTTP_PORT: 8000
      API_HOST: "api"
      API_PORT: 9000
    depends_on:
     - api
    restart: always

  api:
    image: elkozmon/zoonavigator-api:0.5.0
    container_name: zoonavigator-api
    environment:
      API_HTTP_PORT: 9000
    restart: always

networks:
  br17216:
    external:
      name: br17216
```
### 启动zookeeper集群
```
# 先创建网络
docker network create --driver bridge --subnet 172.16.0.0/24 --gateway 172.16.0.1 br17216
# 进入docker-compose.yml所在的目录
docker-compose up -d
```
### kafka集群的docker-compose.yml
```
version: '2'

services:
  kafka1:
    image: wurstmeister/kafka
    restart: always
    hostname: kafka1
    container_name: kafka1
    ports:
     - 9192:9092
    volumes:
     - /home/xdxa/kafkacluster/kafka1/logs:/kafka
    environment:
      KAFKA_ADVERTISED_HOST_NAME: kafka1
      KAFKA_ADVERTISED_PORT: 9092
      KAFKA_ZOOKEEPER_CONNECT: zoo1:2181,zoo2:2181,zoo3:2181
    external_links:
     - zoo1
     - zoo2
     - zoo3
    networks:
      br17216:
        ipv4_address: 172.16.0.14

  kafka2:
    image: wurstmeister/kafka
    restart: always
    hostname: kafka2
    container_name: kafka2
    ports:
     - 9193:9093
    volumes:
     - /home/xdxa/kafkacluster/kafka2/logs:/kafka
    environment:
      KAFKA_ADVERTISED_HOST_NAME: kafka2
      KAFKA_ADVERTISED_PORT: 9093
      KAFKA_ZOOKEEPER_CONNECT: zoo1:2181,zoo2:2181,zoo3:2181
    external_links:
     - zoo1
     - zoo2
     - zoo3
    networks:
      br17216:
        ipv4_address: 172.16.0.15

  kafka3:
    image: wurstmeister/kafka
    restart: always
    hostname: kafka3
    container_name: kafka3
    ports:
     - 9194:9094
    volumes:
     - /home/xdxa/kafkacluster/kafka3/logs:/kafka
    environment:
      KAFKA_ADVERTISED_HOST_NAME: kafka3
      KAFKA_ADVERTISED_PORT: 9094
      KAFKA_ZOOKEEPER_CONNECT: zoo1:2181,zoo2:2181,zoo3:2181
    external_links:
     - zoo1
     - zoo2
     - zoo3
    networks:
      br17216:
        ipv4_address: 172.16.0.16

networks:
  br17216:
    external:
      name: br17216
```

### 启动kafka集群
```
# 进入docker-compose.yml所在目录 
docker-compose up -d
```

## nginx搭建
### 安装编译工具和库文件
```
# 若已安装则忽略
yum -y install make zlib zlib-devel gcc-c++ libtool  openssl openssl-devel
```
### 安装PCRE
```
cd /usr/local/src/
wget http://downloads.sourceforge.net/project/pcre/pcre/8.35/pcre-8.35.tar.gz
tar zxvf pcre-8.35.tar.gz
cd pcre-8.35
 ./configure
 make && make install
```
### 安装nginx
```
cd /usr/local/src/
wget http://nginx.org/download/nginx-1.6.2.tar.gz
tar zxvf nginx-1.6.2.tar.gz
cd nginx-1.6.2
./configure --prefix=/usr/local/webserver/nginx --with-http_stub_status_module --with-http_ssl_module --with-pcre=/usr/local/src/pcre-8.35
make & make install
```
### 安装ngx_kafka_module插件
1. 先安装librdkafka依赖
```
cd /usr/local/src
git clone https://github.com/edenhill/librdkafka
cd librdkafka
yum install -y gcc gcc-c++ pcre-devel zlib-devel
./configure
make && make install
```
2. 整合ngx_kafka_module插件
```
cd /usr/local/src
git clone https://github.com/brg-liuwei/ngx_kafka_module
cd /usr/local/src/nginx-1.16.2
./configure --add-module=/usr/local/src/ngx_kafka_module/
make & make install
```
3. 修改nginx.conf
```
  kafka;
  kafka_broker_list 172.16.0.14:9092	
  server {
  ...
    	location = /kafka/trace {
                kafka_topic tp_individual;
        }
  ...
    }

```
4. 测试命令
```
curl http://192.168.3.39/kafka/trace -d "test from xdx"
```

# 测试

消费代码详见目录 demo-02-springboot-kafka

打包项目, 启动项目
```
java -jar demo-02-springboot-kafka-0.0.1-SNAPSHOT.jar
```



