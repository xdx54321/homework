
## 什么是CAS
CAS（CompareAndSwap），意思是做比较并替换操作。

一共涉及到3个操作数：    
    1.内存地址V
    2.旧的预期值old
    3.即将要更新的目标值target

CAS指令执行时，当且仅当内存地址V的值与预期值old相等时，将内存地址V的值修改为target，否则就什么都不做。

重点是整个比较并替换的操作是一个原子操作,现在处理器都内置该指令且通过硬件级别的设计来保证上述操作是原子性。

实际应用中一般用于保护临界资源, 比如一些多线程访问的共享变量。相对于阻塞性的同步操作(如Synchronized)来说，用CAS性能较好.

适合并发量不是很高且共享资源占用时间较短的场景使用.


## CyclicBarrier和CountDownLatch区别
CountDownLatch采用减计数的方式来实现线程控制，调用countDown()方法计数减一，调用await()方法只进行阻塞，对计数没任何影响，
当计数为0时释放所有等待线程, 且无法重置计数, 不可重复利用.

CountDownLatch采用加计数的方式实现线程控制, 调用await()方法计数加1，若加1后的值不等于new时传入构造方法的值则线程阻塞，
计数达到指定值(构造器传入)时，释放所有等待线程, 计数置为0重新开始, 可重复利用。


## volatile关键字的作用
Java 内存模型(JMM)规定，所有的变量都存放在主内存中，而每个线程都有着自己的工作内存。
线程在工作时，需要将主内存中的数据拷贝到工作内存中进行，并且不能直接操作主内存以及其他线程工作内存中的数据，在操作后要将新数据刷新到主内存中。这叫内存不可见性。
1. **而volatile保证了内存的可见性。**
当一个变量被 volatile 修饰时，任何线程对它的写操作都会立即刷新到主内存中，并且会强制让缓存了该变量的线程中的数据清空，必须从主内存重新读取最新数据。
2. 除了保证内容可见性外, 还可以**防止JVM进行指令重排优化**，比较常用的场景是单例对象通过会用volatile来修饰以保证获取到的对象是已经初始化过的。


## 如何正确的使用wait()?使用if还是while？
**永远在while循环里而不是if语句下使用wait。**

如果采用if判断，当线程从wait中唤醒时，那么将直接执行处理其他业务逻辑的代码，但这时候可能出现另外一种可能，条件谓词已经不满足处理业务逻辑的条件了，从而出现错误的结果. 如果使用while的话，也会从wait之后的代码运行，但是唤醒后会重新判断循环条件，如果不成立再执行while代码块之后的代码块，成立的话继续wait。


## 为什么wait、nofity和nofityAll这些方法不放在Thread类当中
 1. wait 和 notify 用于线程之间通信，线程之间通信的基础是获得或者曾经获得的是同一个锁，
而在 Java ，锁也是一种对象，这两个线程的通信要依赖于这个对象才行.
 2. 如果不放在对象上，虽然也可以用Thread.wait(Object o)的方法把对象作为参数传进去，
但从面向对象设计的角度比较适合将这些方法直接放在对象上。


## synchronized和ReentrantLock的区别
1. 对于Synchronized来说，它是java语言的关键字，是原生语法层面的互斥，需要jvm实现。而ReentrantLock它是JDK提供的API层面的互斥锁，需要lock()和unlock()方法配合try/finally语句块来完成。
2. synchronized既可以修饰方法，也可以修饰代码块。lock只可修饰代码块。
3. 用了Synchronized的方法或者代码块有等待不可中断性，会一直阻塞等待直到别的线程释放。 使用ReentrantLock则是等待中断，线程等待超过一定时间可中断等待。
4. synchronized的锁是非公平锁，而ReentrantLock可以设置成公平也可设置成非公平，比较灵活。
5. ReentrantLock可绑定多个条件

## 什么是AQS
AQS 全称是 AbstractQueuedSynchronizer，是一个用来构建锁和同步器的框架，底层用了 CAS 技术来保证操作的原子性，同时利用 FIFO 队列实现线程间的锁竞争，将基础的同步相关抽象细节放在 AQS，这也是 ReentrantLock、CountDownLatch 等同步工具实现同步的底层实现机制。

## 什么是Java内存模型
Java内存模型（Java Memory Model，JMM）是Java虚拟机规范中的定义，其目的是用于屏蔽掉各种硬件和操作系统的内存访问差异，以实现让Java程序在各种平台下都能达到一致的并发效果。

Java 内存模型中规定了所有的变量都存储在主内存中，每个线程还有自己的工作内存（类比缓存理解），线程的工作内存中保存了该线程使用到主内存中的变量拷贝，线程对变量的所有操作（读取、赋值）都必须在工作内存中进行，而不能直接读写主内存中的变量。不同线程之间无法直接访问对方工作内存中的变量，线程间变量值的传递（通信）均需要在主内存来完成.

从内存区分角度来看，JMM将内存区域主要分为 Java堆，虚拟机栈，方法区，本地方法栈，程序计数器。


## 什么是自旋
**自旋本质是循环等待资源。**

比如自旋锁（spinlock）：是指当一个线程在获取锁的时候，如果锁已经被其它线程获取，那么该线程将循环等待，然后不断的判断锁是否能够被成功获取，直到获取到锁才会退出循环。
自旋锁与互斥锁都是解决对某项资源的互斥使用问题。任何时刻锁都最多只能有一个保持者，所以在任何时刻只能有一个执行单元(如线程)获得锁。
但是两者在调度机制上略有不同。对于互斥锁而言，如果资源已经被占用，则资源申请者只能进入睡眠状态。
但是自旋锁不会引起调用者睡眠，如果自旋锁已经被别的执行单元占据，调用者就一直循环在那里看是否该自旋锁的保持者已经释放了锁，"自旋"一词就是因此而得名。
自旋锁适合锁等待周期短的场景,  等待时间小于线程上下文切换的时间(参考值3.8us)。


## Java编程写一个会导致死锁的程序
```
public class DeadLockSample extends Thread {


    private String first;
    private String second;


    public DeadLockSample(String name, String first, String second) {
        super(name);
        this.first = first;
        this.second = second;
    }


    @Override
    public void run() {
        synchronized (first) {
            System.out.println(this.getName() + " obtained " + first);
            try {
                Thread.sleep(1000L);
                synchronized (second) {
                    System.out.println(this.getName() + " obtained " + second);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args) throws InterruptedException {
        String lockA = "lockA";
        String lockB = "lockB";
        DeadLockSample t1 = new DeadLockSample("Thread1", lockA, lockB);
        DeadLockSample t2 = new DeadLockSample("Thread2", lockB, lockA);
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }

}
```