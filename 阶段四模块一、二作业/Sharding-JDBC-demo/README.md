# 运行环境
### MySQL: Linux & Docker & Docker-Compose
### 代码: MacOS & jdk1.8

# 视频演示地址

链接: https://pan.baidu.com/s/1C5947enhn2_4WRvxO1ClzA  密码: ut6l

# 项目结构
### 代码
```
src/main
com.lagou.demo.jdbc
    dao
        OrderMapper.java     
    entity
        Order.java
    Application.java          # 启动类
src/test
com.lagou.demo.jdbc.dao
    OrderMapperTest.java      # 单元测试类
```
### MySQL集群的Docker-compose配置
```
├── cluster1
   ├── mysql-master
   │   ├── conf
   │   │   └── my.cnf
   │   └── data
   ├── mysql-slave1
   │   ├── conf
   │   │   └── my.cnf
   │   └── data
   └── mysql-slave2
       ├── conf
       │   └── my.cnf
       └── data
├── cluster2
   ├── mysql1-master
   │   ├── conf
   │   │   └── my.cnf
   │   └── data
   ├── mysql1-slave1
   │   ├── conf
   │   │   └── my.cnf
   │   └── data
   └── mysql1-slave2
       ├── conf
       │   └── my.cnf
       └── data
```

# 部署节点
|  节点   | 用途  |
|  ----  | ----  |
| mysql-master  | Cluster1 - MySQL Master |
| mysql-slave1  | Cluster1 - MySQL Slave |
| mysql-slave1  | Cluster1 - MySQL Slave |
| mysql1-master  | Cluster2 - MySQL Master |
| mysql1-slave1  | Cluster2 - MySQL Slave |
| mysql1-slave1  | Cluster2 - MySQL Slave |

# 数据库配置（my.cnf）
### 主库配置
```
!includedir /etc/mysql/conf.d/
!includedir /etc/mysql/mysql.conf.d/

[mysqld]
# 设置server_id，一般设置为IP，注意要唯一
server_id=100
# 复制过滤：也就是指定哪个数据库不用同步（mysql库一般不同步）
binlog-ignore-db=mysql
# 开启二进制日志功能，可以随便取，最好有含义（关键就是这里了）
log-bin=replicas-mysql-bin
# 为每个session分配的内存，在事务过程中用来存储二进制日志的缓存
binlog_cache_size=1M
# 主从复制的格式（mixed,statement,row，默认格式是statement）
binlog_format=mixed
# 二进制日志自动删除/过期的天数。默认值为0，表示不自动删除。
expire_logs_days=7
# 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave端复制中断。
# 如：1062错误是指一些主键重复，1032错误是因为主从数据库数据不一致
slave_skip_errors=1062
```

### 从库1配置
```
!includedir /etc/mysql/conf.d/
!includedir /etc/mysql/mysql.conf.d/

[mysqld]
# 设置server_id，一般设置为IP，注意要唯一
server_id=200
# 复制过滤：也就是指定哪个数据库不用同步（mysql库一般不同步）
binlog-ignore-db=mysql
# 开启二进制日志功能，以备Slave作为其它Slave的Master时使用
log-bin=replicas-mysql-slave1-bin
# 为每个session 分配的内存，在事务过程中用来存储二进制日志的缓存
binlog_cache_size=1M
# 主从复制的格式（mixed,statement,row，默认格式是statement）
binlog_format=mixed
# 二进制日志自动删除/过期的天数。默认值为0，表示不自动删除。
expire_logs_days=7
# 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave端复制中断。
# 如：1062错误是指一些主键重复，1032错误是因为主从数据库数据不一致
slave_skip_errors=1062
# relay_log配置中继日志
relay_log=replicas-mysql-relay-bin
# log_slave_updates表示slave将复制事件写进自己的二进制日志
log_slave_updates=1
# 防止改变数据(除了特殊的线程)
read_only=1
```

### 从库2配置
```
!includedir /etc/mysql/conf.d/
!includedir /etc/mysql/mysql.conf.d/

[mysqld]
# 设置server_id，一般设置为IP，注意要唯一
server_id=300
# 复制过滤：也就是指定哪个数据库不用同步（mysql库一般不同步）
binlog-ignore-db=mysql
# 开启二进制日志功能，以备Slave作为其它Slave的Master时使用
log-bin=replicas-mysql-slave2-bin
# 为每个session 分配的内存，在事务过程中用来存储二进制日志的缓存
binlog_cache_size=1M
# 主从复制的格式（mixed,statement,row，默认格式是statement）
binlog_format=mixed
# 二进制日志自动删除/过期的天数。默认值为0，表示不自动删除。
expire_logs_days=7
# 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave端复制中断。
# 如：1062错误是指一些主键重复，1032错误是因为主从数据库数据不一致
slave_skip_errors=1062
# relay_log配置中继日志
relay_log=replicas-mysql-relay-bin
# log_slave_updates表示slave将复制事件写进自己的二进制日志
log_slave_updates=1
# 防止改变数据(除了特殊的线程)
read_only=1
```

### 创建容器网络
```
docker network create --driver bridge --subnet 10.5.0.0/16 --gateway 10.5.0.1 net1
```

# 运行步骤(以cluster1集群为例)
1. 运行容器, 进入cluster1目录运行下面命令
```cmd
docker-compose up -d
```

2. 配置主库
先进入主库容器中
```cmd
docker exec -it mysql-master bash
mysql -uroot -pRoot123456

```
接着执行下面SQL
```sql
CREATE USER 'slave1'@'%' IDENTIFIED WITH 'mysql_native_password' BY 'slave111';
GRANT REPLICATION SLAVE,REPLICATION CLIENT ON *.* TO 'slave1'@'%';

CREATE USER 'slave2'@'%' IDENTIFIED WITH 'mysql_native_password' BY 'slave222';
GRANT REPLICATION SLAVE,REPLICATION CLIENT ON *.* TO 'slave2'@'%';
```
检查主库状态, 记录返回值
```
show master status
```
本次示例结果:
```
replicas-mysql-bin.000003 |     1084 |
```

3. 配置从库1

进入从库容器
```
docker exec -it mysql-slave1 bash
mysql -uroot -pRoot123456
```
执行下面SQL
```
stop slave;

change master to master_host='10.5.0.5',
master_user='slave1',
master_password='slave111',
master_port=3306,
master_log_file='replicas-mysql-bin.000003',
master_log_pos= 1084,
master_connect_retry=30;

start slave;
```

4. 配置从库2
进入从库容器
```
docker exec -it mysql-slave2 bash
mysql -uroot -pRoot123456
```
执行下面SQL
```
stop slave;

change master to master_host='10.5.0.5',
master_user='slave2',
master_password='slave222',
master_port=3306,
master_log_file='replicas-mysql-bin.000003',
master_log_pos= 1084,
master_connect_retry=30;

start slave;
```

# 验证步骤
1. 分别在两个集群的主库新建表
```sql
CREATE TABLE `c_order`
(
    `id`              bigint(20)   NOT NULL AUTO_INCREMENT,
    `is_del`          bit(1)       NOT NULL DEFAULT 0 COMMENT '是否被删 除',
    `user_id`         int(11)      NOT NULL COMMENT '⽤户id',
    `company_id`      int(11)      NOT NULL COMMENT '公司id',
    `publish_user_id` int(11)      NOT NULL COMMENT 'B端⽤户id',
    `position_id`     int(11)      NOT NULL COMMENT '职位ID',
    `resume_type`     int(2)       NOT NULL DEFAULT 0 COMMENT '简历类型： 0附件 1在线',
    `status`          varchar(256) NOT NULL COMMENT '投递状态 投递状态 WAIT-待处理 AUTO_FILTER-⾃动过滤 PREPARE_CONTACT-待沟通 REFUSE-拒绝 ARRANGE_INTERVIEW-通知⾯试',
    `create_time`     datetime     NOT NULL COMMENT '创建时间',
    `update_time`     datetime     NOT NULL COMMENT '处理时间',
    PRIMARY KEY (`id`),
    KEY `index_userId_positionId` (`user_id`, `position_id`),
    KEY `idx_userId_operateTime` (`user_id`, `update_time`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
```

2. 执行代码中的单元测试插入10条数据并检查结果
```
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 0; i < 10; i++) {
            // 新增数据
            Order order = new Order();
            order.setCompanyId(random.nextInt(1000, 999999));
            order.setUserId(random.nextInt(1000, 999999));
            order.setPositionId(random.nextInt(1000, 999999));
            order.setPublishUserId(random.nextInt(1000, 999999));
            order.setResumeType(random.nextBoolean() ? 1 : 0);
            order.setStatus("xdx");
            order.setIsDel((byte) 0);
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            // 查询数据
            orderMapper.save(order);
        }
        List<Order> orders = orderMapper.findAll();
        log.info("{}", orders);```
```
3. 关键日志执行结果
```
2020-07-24 16:57:19.722  INFO 61230 --- [           main] j.LocalContainerEntityManagerFactoryBean : Initialized JPA EntityManagerFactory for persistence unit 'default'
2020-07-24 16:57:20.013  INFO 61230 --- [           main] com.lagou.demo.jdbc.dao.OrderMapperTest  : Started OrderMapperTest in 3.399 seconds (JVM running for 3.917)
2020-07-24 16:57:20.516  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.516  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@8aa5ab4), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@8aa5ab4, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[800685, 2020-07-24 16:57:20.05, 0, 632866, 760767, 1, xdx, 2020-07-24 16:57:20.05, 807919])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.516  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master1 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [800685, 2020-07-24 16:57:20.05, 0, 632866, 760767, 1, xdx, 2020-07-24 16:57:20.05, 807919]
2020-07-24 16:57:20.624  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.624  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@5a6efe33), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@5a6efe33, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[676907, 2020-07-24 16:57:20.623, 0, 810321, 187130, 0, xdx, 2020-07-24 16:57:20.623, 774975])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.624  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master1 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [676907, 2020-07-24 16:57:20.623, 0, 810321, 187130, 0, xdx, 2020-07-24 16:57:20.623, 774975]
2020-07-24 16:57:20.640  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.640  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@5af40e45), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@5af40e45, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[303540, 2020-07-24 16:57:20.64, 0, 848894, 339661, 0, xdx, 2020-07-24 16:57:20.64, 504905])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.640  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master0 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [303540, 2020-07-24 16:57:20.64, 0, 848894, 339661, 0, xdx, 2020-07-24 16:57:20.64, 504905]
2020-07-24 16:57:20.658  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.658  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@3b56947a), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@3b56947a, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[974151, 2020-07-24 16:57:20.657, 0, 324877, 921764, 0, xdx, 2020-07-24 16:57:20.657, 267375])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.658  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master0 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [974151, 2020-07-24 16:57:20.657, 0, 324877, 921764, 0, xdx, 2020-07-24 16:57:20.657, 267375]
2020-07-24 16:57:20.671  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.671  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@5c60c08), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@5c60c08, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[820992, 2020-07-24 16:57:20.67, 0, 922040, 140156, 0, xdx, 2020-07-24 16:57:20.67, 917335])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.671  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master0 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [820992, 2020-07-24 16:57:20.67, 0, 922040, 140156, 0, xdx, 2020-07-24 16:57:20.67, 917335]
2020-07-24 16:57:20.685  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.685  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@638d2ce3), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@638d2ce3, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[552799, 2020-07-24 16:57:20.684, 0, 225462, 931369, 0, xdx, 2020-07-24 16:57:20.684, 80740])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.685  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master1 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [552799, 2020-07-24 16:57:20.684, 0, 225462, 931369, 0, xdx, 2020-07-24 16:57:20.684, 80740]
2020-07-24 16:57:20.703  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.703  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@4f552aa2), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@4f552aa2, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[849618, 2020-07-24 16:57:20.702, 0, 789732, 262358, 0, xdx, 2020-07-24 16:57:20.702, 544498])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.703  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master1 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [849618, 2020-07-24 16:57:20.702, 0, 789732, 262358, 0, xdx, 2020-07-24 16:57:20.702, 544498]
2020-07-24 16:57:20.726  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.726  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@1d2c253), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@1d2c253, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[735420, 2020-07-24 16:57:20.724, 0, 875350, 717660, 0, xdx, 2020-07-24 16:57:20.724, 631238])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.727  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master1 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [735420, 2020-07-24 16:57:20.724, 0, 875350, 717660, 0, xdx, 2020-07-24 16:57:20.724, 631238]
2020-07-24 16:57:20.742  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.742  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@41bd6a0f), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@41bd6a0f, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[824251, 2020-07-24 16:57:20.741, 0, 893608, 337371, 0, xdx, 2020-07-24 16:57:20.741, 444480])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.742  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master1 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [824251, 2020-07-24 16:57:20.741, 0, 893608, 337371, 0, xdx, 2020-07-24 16:57:20.741, 444480]
2020-07-24 16:57:20.760  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
2020-07-24 16:57:20.760  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: InsertStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.InsertStatement@2cdb5974, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@67e4928c), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@67e4928c, columnNames=[company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id], insertValueContexts=[InsertValueContext(parametersCount=9, valueExpressions=[ParameterMarkerExpressionSegment(startIndex=135, stopIndex=135, parameterMarkerIndex=0), ParameterMarkerExpressionSegment(startIndex=138, stopIndex=138, parameterMarkerIndex=1), ParameterMarkerExpressionSegment(startIndex=141, stopIndex=141, parameterMarkerIndex=2), ParameterMarkerExpressionSegment(startIndex=144, stopIndex=144, parameterMarkerIndex=3), ParameterMarkerExpressionSegment(startIndex=147, stopIndex=147, parameterMarkerIndex=4), ParameterMarkerExpressionSegment(startIndex=150, stopIndex=150, parameterMarkerIndex=5), ParameterMarkerExpressionSegment(startIndex=153, stopIndex=153, parameterMarkerIndex=6), ParameterMarkerExpressionSegment(startIndex=156, stopIndex=156, parameterMarkerIndex=7), ParameterMarkerExpressionSegment(startIndex=159, stopIndex=159, parameterMarkerIndex=8)], parameters=[679333, 2020-07-24 16:57:20.759, 0, 697809, 431718, 0, xdx, 2020-07-24 16:57:20.759, 759792])], generatedKeyContext=Optional.empty)
2020-07-24 16:57:20.760  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: master0 ::: insert into c_order (company_id, create_time, is_del, position_id, publish_user_id, resume_type, status, update_time, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ::: [679333, 2020-07-24 16:57:20.759, 0, 697809, 431718, 0, xdx, 2020-07-24 16:57:20.759, 759792]
2020-07-24 16:57:20.812  INFO 61230 --- [           main] o.h.h.i.QueryTranslatorFactoryInitiator  : HHH000397: Using ASTQueryTranslatorFactory
2020-07-24 16:57:20.910  INFO 61230 --- [           main] ShardingSphere-SQL                       : Logic SQL: select order0_.id as id1_0_, order0_.company_id as company_2_0_, order0_.create_time as create_t3_0_, order0_.is_del as is_del4_0_, order0_.position_id as position5_0_, order0_.publish_user_id as publish_6_0_, order0_.resume_type as resume_t7_0_, order0_.status as status8_0_, order0_.update_time as update_t9_0_, order0_.user_id as user_id10_0_ from c_order order0_
2020-07-24 16:57:20.910  INFO 61230 --- [           main] ShardingSphere-SQL                       : SQLStatement: SelectStatementContext(super=CommonSQLStatementContext(sqlStatement=org.apache.shardingsphere.sql.parser.sql.statement.dml.SelectStatement@5ffd35dd, tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@311a09b2), tablesContext=org.apache.shardingsphere.sql.parser.binder.segment.table.TablesContext@311a09b2, projectionsContext=ProjectionsContext(startIndex=7, stopIndex=344, distinctRow=false, projections=[ColumnProjection(owner=order0_, name=id, alias=Optional[id1_0_]), ColumnProjection(owner=order0_, name=company_id, alias=Optional[company_2_0_]), ColumnProjection(owner=order0_, name=create_time, alias=Optional[create_t3_0_]), ColumnProjection(owner=order0_, name=is_del, alias=Optional[is_del4_0_]), ColumnProjection(owner=order0_, name=position_id, alias=Optional[position5_0_]), ColumnProjection(owner=order0_, name=publish_user_id, alias=Optional[publish_6_0_]), ColumnProjection(owner=order0_, name=resume_type, alias=Optional[resume_t7_0_]), ColumnProjection(owner=order0_, name=status, alias=Optional[status8_0_]), ColumnProjection(owner=order0_, name=update_time, alias=Optional[update_t9_0_]), ColumnProjection(owner=order0_, name=user_id, alias=Optional[user_id10_0_])]), groupByContext=org.apache.shardingsphere.sql.parser.binder.segment.select.groupby.GroupByContext@4cacccbf, orderByContext=org.apache.shardingsphere.sql.parser.binder.segment.select.orderby.OrderByContext@301d84f6, paginationContext=org.apache.shardingsphere.sql.parser.binder.segment.select.pagination.PaginationContext@6f80cf5, containsSubquery=false)
2020-07-24 16:57:20.910  INFO 61230 --- [           main] ShardingSphere-SQL                       : Actual SQL: slave2 ::: select order0_.id as id1_0_, order0_.company_id as company_2_0_, order0_.create_time as create_t3_0_, order0_.is_del as is_del4_0_, order0_.position_id as position5_0_, order0_.publish_user_id as publish_6_0_, order0_.resume_type as resume_t7_0_, order0_.status as status8_0_, order0_.update_time as update_t9_0_, order0_.user_id as user_id10_0_ from c_order order0_
2020-07-24 16:57:20.956  INFO 61230 --- [           main] com.lagou.demo.jdbc.dao.OrderMapperTest  : [Order(id=1, isDel=0, userId=11, companyId=1, publishUserId=123, positionId=234, resumeType=1, status=xdx, createTime=2020-07-24 16:43:45.0, updateTime=2020-07-24 16:43:45.0), Order(id=2, isDel=0, userId=807919, companyId=800685, publishUserId=760767, positionId=632866, resumeType=1, status=xdx, createTime=2020-07-24 16:57:20.0, updateTime=2020-07-24 16:57:20.0), Order(id=3, isDel=0, userId=774975, companyId=676907, publishUserId=187130, positionId=810321, resumeType=0, status=xdx, createTime=2020-07-24 16:57:21.0, updateTime=2020-07-24 16:57:21.0), Order(id=4, isDel=0, userId=80740, companyId=552799, publishUserId=931369, positionId=225462, resumeType=0, status=xdx, createTime=2020-07-24 16:57:21.0, updateTime=2020-07-24 16:57:21.0), Order(id=5, isDel=0, userId=544498, companyId=849618, publishUserId=262358, positionId=789732, resumeType=0, status=xdx, createTime=2020-07-24 16:57:21.0, updateTime=2020-07-24 16:57:21.0), Order(id=6, isDel=0, userId=631238, companyId=735420, publishUserId=717660, positionId=875350, resumeType=0, status=xdx, createTime=2020-07-24 16:57:21.0, updateTime=2020-07-24 16:57:21.0), Order(id=7, isDel=0, userId=444480, companyId=824251, publishUserId=337371, positionId=893608, resumeType=0, status=xdx, createTime=2020-07-24 16:57:21.0, updateTime=2020-07-24 16:57:21.0)]
```


