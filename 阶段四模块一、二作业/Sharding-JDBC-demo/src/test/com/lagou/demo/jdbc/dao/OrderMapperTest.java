package com.lagou.demo.jdbc.dao;

import com.lagou.demo.jdbc.Application;
import com.lagou.demo.jdbc.entity.Order;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Slf4j
public class OrderMapperTest {

    @Resource
    private OrderMapper orderMapper;

    @Test
    public void test() {

        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 0; i < 10; i++) {
            // 新增数据
            Order order = new Order();
            order.setCompanyId(random.nextInt(1000, 999999));
            order.setUserId(random.nextInt(1000, 999999));
            order.setPositionId(random.nextInt(1000, 999999));
            order.setPublishUserId(random.nextInt(1000, 999999));
            order.setResumeType(random.nextBoolean() ? 1 : 0);
            order.setStatus("xdx");
            order.setIsDel((byte) 0);
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            // 查询数据
            orderMapper.save(order);
        }
        List<Order> orders = orderMapper.findAll();
        log.info("{}", orders);
    }

}
