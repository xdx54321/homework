package com.lagou.demo.jdbc.dao;

import com.lagou.demo.jdbc.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderMapper extends JpaRepository<Order, Long> {

}
