# 运行环境
### Linux
### Docker & Docker-Compose

# 视频演示地址

链接: https://pan.baidu.com/s/1vL-zo4__aS8WCn-BUY_cXQ  密码: 8od0

# 项目结构
```
L--mysql-mha-docker                //主目录
    L--scripts                     //本地（Docker宿主）使用的一些脚本
        L--mha_check_repl.sh
        L--...
    L--services                    //需要build的服务（目前是空）
    L--volumes                     //各个容器的挂载数据卷
        L--mha_manager
        L--mha_node0
        L--mha_node1
        L--mha_node2
        L--mha_share               //各个容器共享的目录
          L--scripts               //各个容器共用的一些脚本
    L--parameters.env              //账号密码等环境参数
    L--docker-compose.yml          //编排配置
```

# 部署节点
|  节点   | 用途  |
|  ----  | ----  |
| mha_node0  | MySQL Master |
| mha_node1  | MySQL Slave |
| mha_node2  | MySQL Slave |
| mha_manager  | MHA Manager |

# 数据库配置（my.cnf）
### 主库配置
```
[mysqld]
server-id=1
log-bin=mysql-bin
binlog-do-db=testing
binlog-ignore-db=mysql
replicate-do-db=testing
replicate-ignore-db=mysql
auto_increment_increment=2
auto_increment_offset=1
expire_logs_days=7
```

### 备库配置
```
[mysqld]
server-id=2
log-bin=mysql-bin
binlog-do-db=testing
binlog-ignore-db=mysql
replicate-do-db=testing
replicate-ignore-db=mysql
auto_increment_increment=2
auto_increment_offset=2
expire_logs_days=7
```

### 从库配置
```
[mysqld]
server-id=3
replicate-do-db=testing
replicate-ignore-db=mysql
expire_logs_days=7
```

### MHA配置
```
[server default]
user=root
password=123456
ssh_user=root

manager_workdir=/usr/local/mha
remote_workdir=/usr/local/mha

repl_user=myslave
repl_password=myslave

[server0]
hostname=10.5.0.10

[server1]
hostname=10.5.0.11

[server2]
hostname=10.5.0.12
```

# 运行步骤
1. 运行容器
```cmd
$ docker-compose up -d

```

2. 启动镜像内的SSH服务
```cmd
$ sh ./scripts/ssh_start.sh 

```

3. 复制SSH公钥以实现免密登录
```cmd
$ sh ./scripts/ssh_share.sh 

```

4. 设置MySQL主从相关
```cmd
$ sh ./scripts/mysql_set_mbs.sh
```

5. 启动MHA Manager
```cmd
$ sh ./scripts/mha_start_manager.sh

```

# 验证步骤
1. 主库新建表
```sql
drop table if exists t_position;
create table t_position
(
    id     int auto_increment
        primary key,
    name   varchar(32) null,
    salary int         null,
    city   varchar(32) null
)
    comment '职位表';

drop table if exists t_position_detail;
create table t_position_detail
(
    id          int auto_increment
        primary key,
    pid         int  not null,
    description text null
);
```
2. 添加数据，演示半同步复制
- 先查询主从库数据
```
select * from t_position;
```
- 添加后，再次查询主从库数据
```
insert into t_position(name, salary, city) value ('xdx', 999999, 'shenzhen');
select * from t_position;

```
- 查看log，是否显示半同步
```
docker exec -it mha_node2 bash;
mysql -u root -p123456;
show slave status;
```
3. 主机出故障，从库能自动切换功能
- 关闭主服务器，查看MHA服务器是否正常切换
```
docker pause mha_node0
```
- 在切换后的主节点机器上查看状态展示效果

- 在切换后的主节点机器数据库添加数据，分别查询主从库数据一致
```
insert into t_position(name, salary, city) value ('xxx', 88888, 'shanghai');
select * from t_position;
```
