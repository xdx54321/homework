package com.example.blog.myblog.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "t_article")
@Entity
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "categories")
    private String categories;

    @Column(name = "tags")
    private String tags;

    @Column(name = "allow_comment")
    private String allowComment;

    @Column(name = "thumbnail")
    private String thumbnail;

    @Column(name = "created")
    private Date created;

    @Column(name = "modified")
    private Date modified;

}
