package com.example.blog.myblog.service;

import com.example.blog.myblog.dao.ArticleDao;
import com.example.blog.myblog.entity.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ArticleService {

    @Resource
    private ArticleDao articleDao;

    public Page<Article> findByPage(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return articleDao.findAll(pageable);
    }

}
