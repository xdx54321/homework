package com.example.blog.myblog.controller;

import com.example.blog.myblog.entity.Article;
import com.example.blog.myblog.service.ArticleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Controller
@Slf4j
public class ArticleController {

    @Resource
    private ArticleService articleService;

    @RequestMapping("/index")
    public ModelAndView findByPage(Integer page, Integer size) {
        if (page == null) {
            page = 0;
        }
        if (size == null) {
            size = 10;
        }
        log.info("findByPage, page: {}, size: {}", page, size);
        Page<Article> articles = articleService.findByPage(page, size);
        ModelAndView mv = new ModelAndView();
        mv.addObject("articles", articles);
        mv.setViewName("client/index");
        return mv;
    }

}
