package com.example.blog.myblog.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.I0Itec.zkclient.IZkChildListener;
import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

@Configuration
@Slf4j
public class DatasourceConfiguration {

    private static ZkClient zkClient = new ZkClient("127.0.0.1:2181", 5000, 3000);

    @Bean
    public DataSource dataSource() {

        String path = "/myblog/datasource";

        String user = new String((byte[]) zkClient.readData(Constants.DB_USER));
        String url = new String((byte[]) zkClient.readData(Constants.DB_URL));
        String password = new String((byte[]) zkClient.readData(Constants.DB_PASS));
        String driver = new String((byte[]) zkClient.readData(Constants.DB_DRIVER));

        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        dataSource.setUrl(url);
        try {
            dataSource.init();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dataSource;
    }

}
