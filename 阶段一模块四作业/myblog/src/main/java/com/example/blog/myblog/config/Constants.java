package com.example.blog.myblog.config;

public class Constants {

    public static final String DB_USER = "/myblog/datasource/user";
    public static final String DB_PASS = "/myblog/datasource/password";
    public static final String DB_URL = "/myblog/datasource/url";
    public static final String DB_DRIVER = "/myblog/datasource/driver";

}
