package com.example.blog.myblog.dao;

import com.example.blog.myblog.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleDao extends JpaRepository<Article, Integer> {
}
