package com.example.blog.myblog;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.example.blog.myblog.dao"})
//@EntityScan("com.example.blog.myblog.dao")
public class MyblogApplication {

    public static ApplicationContext context;

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(MyblogApplication.class, args);
        context = ctx;
    }

}
