package com.example.blog.myblog.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.example.blog.myblog.MyblogApplication;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;

@Component
@Slf4j
public class DataSourceConfigWatcher {

    @Resource
    private DataSource dataSource;

    private ZkClient zkClient = new ZkClient("127.0.0.1:2181", 5000, 3000);

    private DataSource getDataSource() {
        String path = "/myblog/datasource";

        String user = new String((byte[]) zkClient.readData(path + "/user"));
        String url = new String((byte[]) zkClient.readData(path + "/url"));
        String password = new String((byte[]) zkClient.readData(path + "/password"));
        String driver = new String((byte[]) zkClient.readData(path + "/driver"));

        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        dataSource.setUrl(url);
        try {
            dataSource.init();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dataSource;
    }

    @PostConstruct
    private void subscribeConfigChange() throws InterruptedException {
        String path = "/myblog/datasource";
        if (!zkClient.exists(path)) {
            zkClient.createPersistent(path, true);
        }
        zkClient.subscribeDataChanges(Constants.DB_USER, new IZkDataListener() {
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                log.info("数据变化, path: {}, data: {}", s, o);
                ((DruidDataSource) dataSource).restart();
                ((DruidDataSource) dataSource).setUsername(new String((byte[]) o));
            }

            @Override
            public void handleDataDeleted(String s) throws Exception {
                log.info("数据删除, path: {}", s);
            }
        });
        zkClient.subscribeDataChanges(Constants.DB_PASS, new IZkDataListener() {
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                log.info("数据变化, path: {}, data: {}", s, o);
                ((DruidDataSource) dataSource).setPassword(new String((byte[]) o));
            }

            @Override
            public void handleDataDeleted(String s) throws Exception {
                log.info("数据删除, path: {}", s);
            }
        });
        zkClient.subscribeDataChanges(Constants.DB_URL, new IZkDataListener() {
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                log.info("数据变化, path: {}, data: {}", s, o);
                ((DruidDataSource) dataSource).setUsername(new String((byte[]) o));
            }

            @Override
            public void handleDataDeleted(String s) throws Exception {
                log.info("数据删除, path: {}", s);
            }
        });
        zkClient.subscribeDataChanges(Constants.DB_DRIVER, new IZkDataListener() {
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                log.info("数据变化, path: {}, data: {}", s, o);
                ((DruidDataSource) dataSource).setDriverClassName(new String((byte[]) o));
            }

            @Override
            public void handleDataDeleted(String s) throws Exception {
                log.info("数据删除, path: {}", s);
            }
        });

    }

}
