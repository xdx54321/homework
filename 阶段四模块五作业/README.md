# 作业一 Hadoop

演示地址

链接: https://pan.baidu.com/s/1nvxhbxoDwVUUG9m4lZP7Lw  密码: efgm

## 运行环境
Linux & Docker

## 搭建Hadoop及Hbase环境

### 拉取镜像
```
docker pull registry.cn-hangzhou.aliyuncs.com/hxhen/public:v1
```
### 启动容器
开三个终端，分别执行下面三条命令，创建master、slave01、slave02三个节点
```
docker run -itd -h master -p 2181:2181 -p 8080:8080 -p 8085:8085 -p 9090:9090 -p 9000:9000 -p 9095:9095 -p 16000:16000 -p 16010:16010 -p 16201:16201 -p 16301:16301 -p 16020:16020 --name master registry.cn-hangzhou.aliyuncs.com/hxhen/public:v1 /bin/bash
docker run -itd -h slave01 --name slave01 registry.cn-hangzhou.aliyuncs.com/hxhen/public:v1 /bin/bash
docker run -itd -h slave02 --name slave02 registry.cn-hangzhou.aliyuncs.com/hxhen/public:v1 /bin/bash
```

### 修改配置
容器创建完成后进入容器，分别在三个容器内进行下面添加host配置
```
vim /etc/hosts
172.17.0.2      master
172.17.0.3      slave01
172.17.0.4      slave02
```
修改myid文件
分别在三个容器终端执行下面这条命令，第一行命令在master节点执行，第二行命令在slave01节点执行，第三行命令在slave02节点执行
```
echo "0" > /usr/local/zookeeper/data/myid
echo "1" > /usr/local/zookeeper/data/myid
echo "2" > /usr/local/zookeeper/data/myid
```

### 启动集群
启动顺序为 hadoop -> zookeeper -> hbase

1. 初始化namenode
在master节点执行
```
hadoop namenode -format
```
2. 启动hadoop
在master节点执行
```
start-all.sh
```
3. 启动zookeeper
分别在三个终端执行
```
zkServer.sh start
```
4. 启动hbase
在master节点执行
```
start-hbase.sh
```

### 查看集群
分别在三个终端执行 jps 命令可以查看当前节点的进行情况

master节点
```
2256 NameNode
3377 Jps
2933 QuorumPeerMain
3126 HMaster
2470 SecondaryNameNode
2650 ResourceManager
```

slave01节点
```
539 QuorumPeerMain
284 DataNode
636 HRegionServer
814 Jps
399 NodeManager
```

slave02节点
```
432 HRegionServer
342 QuorumPeerMain
87 DataNode
617 Jps
202 NodeManager
```

## 运行代码

代码详见 numsort 目录

### 准备
分别编辑并保存file1、file2、file3到hdfs
```
hdfs dfs -mkdir /xdx
hdfs dfs -put file*.txt /xdx
```
提交计算任务
```
hadoop jar sort.jar com.lagou.mr.Sort /xdx/file1.txt /xdx/file2.txt /xdx/file3.txt /xdx/result
```

### 运行结果
查看文件
```
hdfs dfs -text /xdx/result/part-r-00000
```
显示结果
```
1 6
2 15
3 22
4 26
5 32
6 54
7 92
8 232
9 650
10 654
11 756
12 5956
13 65223
```


# 作业二 Hbase

演示视频地址  
链接: https://pan.baidu.com/s/19KcXPAuK-bJJfOm-OJu6jA  密码: ih03

## 要求
1. 使用Hbase相关API创建一张结构如上的表
2. 删除好友操作实现（好友关系双向，一方删除好友，另一方也会被迫删除好友）
例如：uid1用户执行删除uid2这个好友，则uid2的好友列表中也必须删除uid1

## 运行代码

代码详见 hbase_client 目录

### 将代码打包并放到master容器中, 并进入该容器对应的路径执行下面命令
```
java -cp hbase_client.jar com.lagou.hbase.client.HbaseClientDemo
```

### 运行结果
```
init complete
user_relation table create success, press any key to continue ...

table init complete: user_relation
rowkey-->101--;cf-->friends;column--->102--;value-->
rowkey-->101--;cf-->friends;column--->103--;value-->
rowkey-->101--;cf-->friends;column--->104--;value-->
rowkey-->102--;cf-->friends;column--->101--;value-->
rowkey-->102--;cf-->friends;column--->104--;value-->
rowkey-->103--;cf-->friends;column--->101--;value-->
rowkey-->103--;cf-->friends;column--->104--;value-->
rowkey-->104--;cf-->friends;column--->101--;value-->
rowkey-->104--;cf-->friends;column--->103--;value-->
press any key to continue ...

now userId=[101] remove relation with userId=[102]
success remove relation from userId=[101] to userId=[102], now the content of table is:
rowkey-->101--;cf-->friends;column--->103--;value-->
rowkey-->101--;cf-->friends;column--->104--;value-->
rowkey-->102--;cf-->friends;column--->104--;value-->
rowkey-->103--;cf-->friends;column--->101--;value-->
rowkey-->103--;cf-->friends;column--->104--;value-->
rowkey-->104--;cf-->friends;column--->101--;value-->
rowkey-->104--;cf-->friends;column--->103--;value-->
```






