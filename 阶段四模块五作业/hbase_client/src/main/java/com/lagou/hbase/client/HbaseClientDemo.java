package com.lagou.hbase.client;

import com.lagou.hbase.entity.UserRelation;
import io.netty.util.internal.ThreadLocalRandom;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

public class HbaseClientDemo {

    static Configuration conf = null;
    static Connection conn = null;

    static String TABLE_NAME = "user_relation";

    static String FAMILY = "friends";

    public static void init() throws IOException {
        //获取一个配置文件对象
        conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "master,slave01,slave02");
        conf.set("hbase.zookeeper.property.clientPort", "2181");
        //通过conf获取到hbase集群的连接
        conn = ConnectionFactory.createConnection(conf);
    }

    public static void main(String[] args) throws IOException {
        init();
        System.out.println("init complete");
        createTable(TABLE_NAME, FAMILY);
        System.out.println(TABLE_NAME + " table create success, press any key to continue ...");
        Scanner sc = new Scanner(System.in);
        sc.nextLine();

        /*
         * 101  102  103  104
         * 102  101  104
         * 103  104  101
         * 104  101  103
         */
        {
            UserRelation relation = new UserRelation();
            relation.setUserId("101");
            List<String> relationUserIds = new ArrayList<String>(Arrays.asList("102", "103", "104"));
            relation.setRelationUserId(relationUserIds);
            putData(TABLE_NAME, FAMILY, relation);
        }
        {
            UserRelation relation = new UserRelation();
            relation.setUserId("102");
            List<String> relationUserIds = new ArrayList<String>(Arrays.asList("101", "104"));
            relation.setRelationUserId(relationUserIds);
            putData(TABLE_NAME, FAMILY, relation);
        }
        {
            UserRelation relation = new UserRelation();
            relation.setUserId("103");
            List<String> relationUserIds = new ArrayList<String>(Arrays.asList("104", "101"));
            relation.setRelationUserId(relationUserIds);
            putData(TABLE_NAME, FAMILY, relation);
        }
        {
            UserRelation relation = new UserRelation();
            relation.setUserId("104");
            List<String> relationUserIds = new ArrayList<String>(Arrays.asList("101", "103"));
            relation.setRelationUserId(relationUserIds);
            putData(TABLE_NAME, FAMILY, relation);
        }
        System.out.println("table init complete: " + TABLE_NAME);
        scanData(TABLE_NAME);
        System.out.println("press any key to continue ...");
        sc.nextLine();

        System.out.println(String.format("now userId=[%s] remove relation with userId=[%s]", "101", "102"));
        removeRelation("101", "102");
        System.out.println(String.format("success remove relation from userId=[%s] to userId=[%s], now the content of table is: ", "101", "102"));
        scanData(TABLE_NAME);

    }

    public static void createTable(String tableName, String... families) throws IOException {
        HBaseAdmin admin = (HBaseAdmin) conn.getAdmin();
        final HTableDescriptor worker = new HTableDescriptor(TableName.valueOf(tableName));
        for (String family : families) {
            worker.addFamily(new HColumnDescriptor(family));
        }
        admin.createTable(worker);
    }

    // 插入一条数据
    public static void putData(String tableName, String family, UserRelation relation) throws IOException {
        //需要获取一个table对象
        final Table worker = conn.getTable(TableName.valueOf(tableName));

        Put put = new Put(relation.getUserId().getBytes());
        for (String userId : relation.getRelationUserId()) {
            put.addColumn(family.getBytes(), userId.getBytes(), "".getBytes());
        }
        worker.put(put);
        worker.close();
    }

    public static void removeRelation(String userId, String friendId) throws IOException {
        final Table worker = conn.getTable(TableName.valueOf(TABLE_NAME));
        final Delete deleteForUser = new Delete(Bytes.toBytes(userId));
        deleteForUser.addColumn(FAMILY.getBytes(), friendId.getBytes());
        worker.delete(deleteForUser);
        Delete deleteForFriend = new Delete(Bytes.toBytes(friendId));
        deleteForFriend.addColumn(FAMILY.getBytes(), userId.getBytes());
        worker.delete(deleteForFriend);
        worker.close();
    }

    //查询数据
    public static void getRelation(String UserId) throws IOException {
        //准备table对象
        final Table worker = conn.getTable(TableName.valueOf(TABLE_NAME));
        //准备get对象
        final Get get = new Get(Bytes.toBytes(UserId));
        //指定查询某个列族或者列
        get.addFamily(Bytes.toBytes(FAMILY));
        //执行查询
        final Result result = worker.get(get);
        //获取到result中所有cell对象
        final Cell[] cells = result.rawCells();
        //遍历打印
        for (Cell cell : cells) {
            final String rowkey = Bytes.toString(CellUtil.cloneRow(cell));
            final String f = Bytes.toString(CellUtil.cloneFamily(cell));
            final String column = Bytes.toString(CellUtil.cloneQualifier(cell));
            final String value = Bytes.toString(CellUtil.cloneValue(cell));

            System.out.println("rowkey-->" + rowkey + "--;cf-->" + f + "---;column--->" + column + "--;value-->" + value);
        }
        worker.close();
    }

    //全表扫描
    public static void scanData(String tableName) throws IOException {
        //准备table对象
        final Table worker = conn.getTable(TableName.valueOf(tableName));
        //准备scan对象
        final Scan scan = new Scan();

        //执行扫描
        final ResultScanner resultScanner = worker.getScanner(scan);
        for (Result result : resultScanner) {
            //获取到result中所有cell对象
            final Cell[] cells = result.rawCells();
            //遍历打印
            for (Cell cell : cells) {
                final String rowkey = Bytes.toString(CellUtil.cloneRow(cell));
                final String f = Bytes.toString(CellUtil.cloneFamily(cell));
                final String column = Bytes.toString(CellUtil.cloneQualifier(cell));
                final String value = Bytes.toString(CellUtil.cloneValue(cell));
                System.out.println("rowkey-->" + rowkey + "--;cf-->" + f + ";column--->" + column + "--;value-->" + value);
            }
        }

        worker.close();
    }

}
