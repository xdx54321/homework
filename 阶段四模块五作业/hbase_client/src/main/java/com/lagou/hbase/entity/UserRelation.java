package com.lagou.hbase.entity;

import java.util.List;

public class UserRelation {

    private String userId;

    private List<String> relationUserId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getRelationUserId() {
        return relationUserId;
    }

    public void setRelationUserId(List<String> relationUserId) {
        this.relationUserId = relationUserId;
    }

    @Override
    public String toString() {
        return "UserRelation{" +
                "userId='" + userId + '\'' +
                ", relationUserId=" + relationUserId +
                '}';
    }
}
