package com.lagou.edu.user.api.service;

import com.lagou.edu.user.api.entity.User;

public interface UserService {

    User getByUserToken(String token);

}
