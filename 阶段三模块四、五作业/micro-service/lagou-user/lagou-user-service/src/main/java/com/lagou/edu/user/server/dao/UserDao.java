package com.lagou.edu.user.server.dao;

import com.lagou.edu.user.server.entity.LagouUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<LagouUser, Long> {

    LagouUser getByUsername(String username);

    LagouUser getByUsernameAndPassword(String username, String password);

}
