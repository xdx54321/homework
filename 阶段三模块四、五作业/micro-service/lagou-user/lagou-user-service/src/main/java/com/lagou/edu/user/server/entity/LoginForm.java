package com.lagou.edu.user.server.entity;

import lombok.Data;

@Data
public class LoginForm {

    private String username;

    private String password;

}
