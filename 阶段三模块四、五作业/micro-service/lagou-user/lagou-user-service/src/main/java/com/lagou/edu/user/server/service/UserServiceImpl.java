package com.lagou.edu.user.server.service;

import com.lagou.edu.code.api.entity.CodeForm;
import com.lagou.edu.code.api.service.CodeService;
import com.lagou.edu.user.api.entity.User;
import com.lagou.edu.user.api.service.UserService;
import com.lagou.edu.user.server.dao.UserDao;
import com.lagou.edu.user.server.entity.LagouUser;
import com.lagou.edu.user.server.entity.LoginForm;
import com.lagou.edu.user.server.entity.RegisterForm;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@RestController
@Slf4j
@RequestMapping("/user")
@RefreshScope
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    @Reference
    private CodeService codeService;

    private final Map<String, LagouUser> tokenMap = new HashMap<>();

    @PostMapping("/register")
    public int register(@RequestBody RegisterForm form) {
        log.info("register: {}", form);
        CodeForm codeForm = new CodeForm();
        codeForm.setCode(form.getCaptcha());
        codeForm.setEmail(form.getEmail());
        if (!codeService.validate(codeForm)) {
            log.info("验证码错误: {}", form);
            return -1;
        }
        LagouUser user = new LagouUser();
        user.setEmail(form.getEmail());
        user.setUsername(form.getUsername());
        user.setPassword(form.getPassword());
        userDao.save(user);
        return 0;
    }

    @PostMapping("/login")
    public Boolean login(@RequestBody LoginForm form, HttpServletResponse response) {
        LagouUser user = userDao.getByUsernameAndPassword(form.getUsername(), form.getPassword());
        if (user == null) {
            return false;
        }
        String uuid = UUID.randomUUID().toString();
        response.addCookie(new Cookie("uuid", uuid));
        tokenMap.put(uuid, user);
        return true;
    }

    @Override
    public User getByUserToken(String token) {
        LagouUser lagouUser = tokenMap.get(token);
        return lagouUser == null ? null : lagouUser.toUser();
    }

    @GetMapping("")
    public String getUsername(@CookieValue("uuid") String uuid) {
        LagouUser lagouUser = tokenMap.get(uuid);
        return lagouUser == null ? null : lagouUser.getUsername();
    }

}
