package com.lagou.edu.user.server.entity;

import lombok.Data;

@Data
public class RegisterForm {

    private String email;

    private String username;

    private String password;

    private String captcha;

}
