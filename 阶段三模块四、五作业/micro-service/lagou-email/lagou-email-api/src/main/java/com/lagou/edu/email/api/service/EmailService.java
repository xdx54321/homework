package com.lagou.edu.email.api.service;

public interface EmailService {

    void send(String email, String code);

}
