package com.lagou.edu.email.server.service;

import com.lagou.edu.email.api.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@Slf4j
@RefreshScope
@Service
public class EmailServiceImpl implements EmailService {

    @Autowired(required = true)
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String from;

    @Override
    public void send(String email, String code) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from); // 邮件发送者
        message.setTo(email); // 邮件接受者
        message.setSubject("注册验证码"); // 主题
        message.setText("你的验证码为: " + code); // 内容
        mailSender.send(message);
        log.info("邮件发送完成, email: {}, code: {}", email, code);
    }

}
