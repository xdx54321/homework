package com.lagou.edu.code.server.service;

import com.lagou.edu.code.api.entity.CodeForm;
import com.lagou.edu.code.api.service.CodeService;
import com.lagou.edu.email.api.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@RestController
@Service
@RequestMapping("/code")
@Slf4j
public class CodeServiceImpl implements CodeService {

    private final Map<String, String> codeMap = new HashMap<>();

    @Reference
    private EmailService emailService;

    @Override
    @PostMapping("/send")
    public void send(@RequestBody CodeForm codeForm) {
        if (codeForm.getEmail() == null) {
            log.warn("email不能为空!");
            return;
        }
        ThreadLocalRandom random = ThreadLocalRandom.current();
        String code = String.valueOf(random.nextInt(123431, 982738));
        codeMap.put(codeForm.getEmail(), code);
        emailService.send(codeForm.getEmail(), code);
    }

    @Override
    public boolean validate(@RequestBody CodeForm codeForm) {
        return codeMap.containsKey(codeForm.getEmail()) &&
                codeMap.get(codeForm.getEmail()).equals(codeForm.getCode());
    }

}
