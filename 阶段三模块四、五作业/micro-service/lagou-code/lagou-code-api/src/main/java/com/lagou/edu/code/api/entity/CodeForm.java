package com.lagou.edu.code.api.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class CodeForm implements Serializable {

    private String code;

    private String email;

}
