package com.lagou.edu.code.api.service;

import com.lagou.edu.code.api.entity.CodeForm;

public interface CodeService {

    void send(CodeForm codeForm);

    boolean validate(CodeForm codeForm);

}
