package com.lagou.edu.gateway.filter;

import com.lagou.edu.user.api.entity.User;
import com.lagou.edu.user.api.service.UserService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 定义全局过滤器，会对所有路由生效
 */
@Component  // 让容器扫描到，等同于注册了
public class CommonFilter implements GlobalFilter, Ordered {

    @Reference
    private UserService userService;

    /**
     * 过滤器核心方法
     * @param exchange 封装了request和response对象的上下文
     * @param chain 网关过滤器链（包含全局过滤器和单路由过滤器）
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 思路：获取客户端ip，判断是否在黑名单中，在的话就拒绝访问，不在的话就放行
        // 从上下文中取出request和response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

//        // 从request对象中获取客户端ip
//        String clientIp = request.getRemoteAddress().getHostName();
//        // 拿着clientIp去黑名单中查询，存在的话就决绝访问
//        String id = request.getPath().value() + "|" + clientIp + "|" + (System.currentTimeMillis() / 1000 / 60);
//        // 用一个数组
//        if (!RECORD.containsKey(id)) {
//            RECORD.clear();
//            RECORD.put(id, new AtomicInteger(0));
//        }
//        // 60秒分成100份，每份600毫秒, 当前时间在哪个600毫秒内呢
//        int cnt = RECORD.get(id).incrementAndGet();
//        if (cnt > 5) {
//            String data = "Request Limit!";
//            response.setStatusCode(HttpStatus.valueOf(405));
//            DataBuffer wrap = response.bufferFactory().wrap(data.getBytes());
//            return response.writeWith(Mono.just(wrap));
//        }

        if (request.getPath().value().contains("user") && request.getMethodValue().equalsIgnoreCase("GET")) {
            List<HttpCookie> cookies = request.getCookies().get("uuid");
            if (cookies == null || cookies.isEmpty()) {
                response.setStatusCode(HttpStatus.valueOf(401));
                DataBuffer wrap = response.bufferFactory().wrap("User Not Login".getBytes());
                return response.writeWith(Mono.just(wrap));
            }
            User user = userService.getByUserToken(cookies.get(0).getValue());
            if (user == null) {
                response.setStatusCode(HttpStatus.valueOf(401));
                DataBuffer wrap = response.bufferFactory().wrap("User Not Login".getBytes());
                return response.writeWith(Mono.just(wrap));
            }
            response.setStatusCode(HttpStatus.valueOf(200));
            DataBuffer wrap = response.bufferFactory().wrap(user.getEmail().getBytes());
            return response.writeWith(Mono.just(wrap));
        }
        // 合法请求，放行，执行后续的过滤器
        return chain.filter(exchange);
    }

//    private static Map<String, AtomicInteger> RECORD = new ConcurrentHashMap<>();

    /**
     * 返回值表示当前过滤器的顺序(优先级)，数值越小，优先级越高
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
