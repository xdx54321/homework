# K8S作业

视频演示地址
链接: https://pan.baidu.com/s/1UxT1nsTgMpUbBGMWQhdLBg  密码: b36e

# 环境搭建

操作环境: Mac & minikube

## 安装基础依赖

### Docker 
点击链接下载并安装
https://www.docker.com/products/docker-desktop
### kubectl
```
brew install kubectl
```
### VirtaulBox
官网下载地址：https://www.virtualbox.org/
安装过程中点"允许", 安装完成后在Preference -> Security&Privacy，把VirtualBox勾上.

## 安装minikube
```
curl -Lo minikube http://kubernetes.oss-cn-hangzhou.aliyuncs.com/minikube/releases/v1.1.1/minikube-darwin-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
```

## 创建minikube集群
```
minikube start --registry-mirror=https://registry.docker-cn.com

# 此时会利用virtualbox创建一个虚拟机, 打开virtaulbox可看到。
# 下面是在本机的命令执行结果
There is a newer version of minikube available (v1.2.0).  Download it here:
https://github.com/kubernetes/minikube/releases/tag/v1.2.0

To disable this notification, run the following:
minikube config set WantUpdateNotification false
😄  minikube v1.1.1 on darwin (amd64)
✅  using image repository registry.cn-hangzhou.aliyuncs.com/google_containers
💿  Downloading Minikube ISO ...
 131.29 MB / 131.29 MB [============================================] 100.00% 0s
🔥  Creating virtualbox VM (CPUs=2, Memory=2048MB, Disk=20000MB) ...


🐳  Configuring environment for Kubernetes v1.14.3 on Docker 18.09.6
💾  Downloading kubelet v1.14.3
💾  Downloading kubeadm v1.14.3
🚜  Pulling images ...
🚀  Launching Kubernetes ...
⌛  Verifying: apiserver proxy etcd scheduler controller dns
🏄  Done! kubectl is now configured to use "minikube"
```

## 测试安装效果
```
kubectl cluster-info
# 本机执行结果:
Kubernetes master is running at https://192.168.99.102:8443
KubeDNS is running at https://192.168.99.102:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```
```
kubectl get nodes -o wide
# 本机执行结果:
NAME       STATUS   ROLES    AGE     VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE            KERNEL-VERSION   CONTAINER-RUNTIME
minikube   Ready    master   7m24s   v1.14.3   10.0.2.15     <none>        Buildroot 2018.05   4.15.0           docker://18.9.6
```
```
# 执行下面命令开启dashboard，执行成功会自动打浏览器
minikube dashboard
# 本机命令行返回执行结果:
🔌  Enabling dashboard ...
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:55129/api/v1/namespaces/kube-system/services/http:kubernetes-dashboard:/proxy/ in your default browser...
```

# 部署服务

## 部署MySQL

### 新建mysql-dp.yaml
```
apiVersion: apps/v1
kind: Deployment  # 类型是部署 
metadata:
  name: mysql-deployment  # 对象的名字
spec:
  selector:
    matchLabels:
      app: mysql #用来绑定label是“mysql”的Pod
  strategy:
    type: Recreate
  template:   # 开始定义Pod 
    metadata:
      labels:
        app: mysql  #Pod的Label，用来标识Pod
    spec:
      containers: # 开始定义Pod里面的容器
        - image: mysql
          name: mysql-con
          imagePullPolicy: Never
          env:   #  定义环境变量
            - name: MYSQL_ROOT_PASSWORD  #  环境变量名
              value: root  #  环境变量值
            - name: MYSQL_USER
              value: dbuser
            - name: MYSQL_PASSWORD
              value: dbuser
          args: ["--default-authentication-plugin=mysql_native_password"]
          ports:
            - containerPort: 3306 # mysql端口 
              name: mysql 
```
### 新建服务配置mysql-svc.yaml
```
apiVersion: v1
kind: Service
metadata:
  name: mysql-service
  labels:
    app: mysql
spec:
  type: NodePort
  selector:
      app: mysql
  ports:
  - protocol : TCP
    nodePort: 30306
    port: 3306
    targetPort: 3306 
```
### 新建数据卷配置文件mysql-pvc.yaml
```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mysql-pv
spec:
  capacity:
    storage: 1Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  storageClassName:  standard #持久卷存储类型，需要与持久卷申请的类型相匹配
  local:
    path: /opt/mysql/data #宿主机的目录
  nodeAffinity:
    required:
      nodeSelectorTerms:
        - matchExpressions:
            - key: kubernetes.io/hostname
              operator: In
              values:
                - minikube 
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pv-claim
  labels:
    app: mysql
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi #1 GB
```
### 提交给K8S执行
```
# 依次执行
kubectl apply -f mysql-pvc.yaml
kubectl apply -f mysql-dp.yaml
kubectl apply -f mysql-svc.yaml
```

## 准备Java项目
将项目打包成可运行的jar包, 代码详见目录.
```
cd demo
mvn clean package
```
### 创建Dockerfile文件
```
FROM openjdk:8-jdk-alpine
RUN mkdir -p /usr/local/lagou
COPY demo-0.0.1-SNAPSHOT.jar  /usr/local/lagou/myapp.jar
ENTRYPOINT ["java","-jar","/usr/local/lagou/myapp.jar"]
```
### 打包镜像
```
# 将jar包和Dockerfile文件传到minikube, 然后构建镜像
# 192.168.99.102是VirtualBox所创建虚拟机的IP地址
scp target/demo-0.0.1-SNAPSHOT.jar docker@192.168.99.102:/home/docker
scp Dockerfile docker@192.168.99.102:/home/docker
docker build -t demo:v1 . 
```
### 新建demo.yaml文件
返回宿主机

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: demo-pod
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: demo-pod
    spec:
      containers:
      - name: demo
        image: demo:v1
        imagePullPolicy: Never
        ports:
        - containerPort: 8080
---
apiVersion: v1
kind: Service
metadata:
  name: demo-svc
spec:
  selector:
      app: demo-pod
  ports:
    - name: http
      port: 8080
      protocol: TCP
      targetPort: 8080
      nodePort: 31000
  type: NodePort
```
### 创建POD
```
kubectl apply -f demo.yaml
```










