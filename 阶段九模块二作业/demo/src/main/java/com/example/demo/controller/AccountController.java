package com.example.demo.controller;

import com.example.demo.entity.Account;
import com.example.demo.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class AccountController {

    @Resource
    private AccountService accountService;

    @GetMapping("/accounts")
    public List<Account> findAll() {
        List<Account> accounts = accountService.findAll();
        return accounts;
    }

    @DeleteMapping("/account")
    public void delete(@RequestParam("username") String username) {
        accountService.delete(username);
    }

    @DeleteMapping("/account/{id}")
    public void deleteById(@PathVariable("id") Integer id) {
        accountService.delete(id);
    }

    @PutMapping("/account")
    public void save(@RequestBody Account account) {
        accountService.save(account);
    }

}
