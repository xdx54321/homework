package com.example.demo.service;

import com.example.demo.dao.AccountDao;
import com.example.demo.entity.Account;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AccountService {

    @Resource
    private AccountDao accountDao;

    public List<Account> findAll() {
        return accountDao.findAll();
    }

    public void delete(String username) {
        Account account = new Account();
        account.setUsername(username);
        accountDao.delete(account);
    }

    public void delete(Integer id) {
        accountDao.deleteById(id);
    }

    public Integer save(Account account) {
        accountDao.save(account);
        return account.getId();
    }

}
