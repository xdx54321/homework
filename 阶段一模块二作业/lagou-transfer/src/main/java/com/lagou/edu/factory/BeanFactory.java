package com.lagou.edu.factory;

import com.lagou.edu.anno.Autowired;
import com.lagou.edu.anno.Repository;
import com.lagou.edu.anno.Service;
import com.lagou.edu.anno.Transactional;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 应癫
 * <p>
 * 工厂类，生产对象（使用反射技术）
 */
public class BeanFactory {

    /**
     * 任务一：读取解析xml，通过反射技术实例化对象并且存储待用（map集合）
     * 任务二：对外提供获取实例对象的接口（根据id获取）
     */
    private static Map<String, Object> map = new HashMap<>();

    static {
        // 任务一：读取解析xml，通过反射技术实例化对象并且存储待用（map集合）
        // 加载xml
        InputStream resourceAsStream = BeanFactory.class.getClassLoader().getResourceAsStream("beans.xml");
        // 解析xml
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            List<Element> beanList = rootElement.selectNodes("//bean");
            for (Element element : beanList) {
                // 处理每个bean元素，获取到该元素的id 和 class 属性
                String id = element.attributeValue("id");
                String clazz = element.attributeValue("class");
                // 通过反射技术实例化对象
                Class<?> aClass = Class.forName(clazz);
                Object o = aClass.newInstance();
                // 存储到map中待用
                map.put(id, o);
            }

            // 实例化完成之后维护对象的依赖关系，检查哪些对象需要传值进入，根据它的配置，我们传入相应的值
            // 有property子元素的bean就有传值需求
            List<Element> propertyList = rootElement.selectNodes("//property");
            // 解析property，获取父元素
            for (Element element : propertyList) {
                // <property name="AccountDao" ref="accountDao"></property>
                String name = element.attributeValue("name");
                String ref = element.attributeValue("ref");

                // 找到当前需要被处理依赖关系的bean
                Element parent = element.getParent();

                // 调用父元素对象的反射功能
                String parentId = parent.attributeValue("id");
                Object parentObject = map.get(parentId);
                // 遍历父对象中的所有方法，找到"set" + name
                Method[] methods = parentObject.getClass().getMethods();
                for (Method method : methods) {
                    // 该方法就是 setAccountDao(AccountDao accountDao)
                    if (method.getName().equalsIgnoreCase("set" + name)) {
                        method.invoke(parentObject, map.get(ref));
                    }
                }

                // 把处理之后的parentObject重新放到map中
                map.put(parentId, parentObject);
            }

            Element componentScan = (Element) rootElement.selectSingleNode("//component-scan");
            if (componentScan != null) {
                String basePkg = componentScan.attribute("base-package").getValue();
                scanPackage(basePkg);
            }

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        // 扫描Transactional注解
        for (Map.Entry<String, Object> beanEntry : map.entrySet()) {
            // 给对象生成代理
            for (Method method : beanEntry.getValue().getClass().getDeclaredMethods()) {
                Transactional anno = method.getAnnotation(Transactional.class);
                if (anno == null) {
                    continue;
                }
                // 给transactional注解的方法的类加上代理
                wrapProxy(beanEntry.getKey(), beanEntry.getValue());
            }
        }

        System.out.println("包扫描成功");

    }

    private static void wrapProxy(String id, Object bean) {
        ProxyFactory proxyFactory = (ProxyFactory) map.get("proxyFactory");
        if (bean.getClass().getInterfaces().length > 0) {
            // 实现了接口, 生成JDK代理
            map.put(id, proxyFactory.getJdkProxy(bean));
        } else {
            // 生成cglib代理
            map.put(id, proxyFactory.getCglibProxy(bean));
        }
    }

    /**
     * 任务二：对外提供获取实例对象的接口（根据id获取）
     *
     * @param id
     * @return
     */
    public static Object getBean(String id) {
        return map.get(id);
    }

    private static void scanPackage(final String packageName) {

        // 扫包、找注解、找到注解先从map检查、

        String packageDirectory = packageName.replaceAll("\\.", "/");

        URL url = BeanFactory.class.getClassLoader().getResource(packageDirectory);
        assert url != null;
        File[] files = new File(url.getFile()).listFiles(file -> {
            if (file.isDirectory()) {
                scanPackage(packageName + "." + file.getName());
            } else {
                //判断文件后缀是否为.
                return file.getName().endsWith(".class");
            }
            return false;
        });
        assert files != null;
        for (File file : files) {
            String fName = file.getName();
            // 截掉.class
            fName = fName.substring(0, fName.lastIndexOf("."));
            // 类名第一个字母小写
            // 全限定类名
            String canonicalName = packageName + "." + fName;
            try {
                //反射构建对象
                Class<?> c = Class.forName(canonicalName);
                // 类上是否有相关注解
                if (c.isAnnotationPresent(Service.class)) {
                    injectField(c);
                }
                if (c.isAnnotationPresent(Repository.class)) {
                    injectField(c);
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 扫描注解类的属性看有没有Autowired有的话尝试从map注入
     * @param clazz
     */
    private static void injectField(Class clazz) throws IllegalAccessException, InstantiationException {
        String id = String.valueOf(clazz.getSimpleName().charAt(0)).toLowerCase() + clazz.getSimpleName().substring(1);
        Object obj = map.get(id);
        if (obj == null) {
            obj = clazz.newInstance();
            map.put(id, obj);
        }
        for (Field field : clazz.getDeclaredFields()) {
            if (!field.isAnnotationPresent(Autowired.class)) {
                continue;
            }
            String key = String.valueOf(field.getName().charAt(0)).toLowerCase() + field.getName().substring(1);
            Object fieldValue = map.get(key);
            if (fieldValue != null) {
                field.setAccessible(true);
                try {
                    field.set(obj, fieldValue);
                } catch (IllegalAccessException ignored) {}
            }
        }
    }

}
