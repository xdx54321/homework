<%@page contentType="text/html;charset=utf-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv=”Content-Type” content=”text/html; charset=utf-8″>
    <title>Bootstrap 实例 - 基本表单</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <form role="form" action="./save" method="post">
        <div class="form-group">
            <label for="resume_name">姓名</label>
            <input type="text" class="form-control" id="resume_name" name="name"
                   placeholder="请输入姓名">
        </div>
        <div class="form-group">
            <label for="resume_address">地址</label>
            <input type="text" class="form-control" id="resume_address" name="address"
                   placeholder="请输入地址">
        </div>
        <div class="form-group">
            <label for="resume_phone">手机</label>
            <input type="text" class="form-control" id="resume_phone" name="phone"
                   placeholder="请输入手机">
        </div>
        <button type="submit" class="btn btn-default">提交</button>
    </form>
</div>
</body>
</html>