<%@ page import="com.lagou.edu.pojo.Resume" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>员工列表</title>

    <!-- 找到web项目的webapp路径 -->
    <%
        pageContext.setAttribute("APP_PATH", request.getContextPath());
    %>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</head>
<body>
<!-- 搭建显示页面 -->
<div class="container">

    <!-- 标题 -->
    <div class="row">
        <div class="col-md-12">
            <h1>管理</h1>
        </div>
    </div>

    <!-- 新增按钮 -->
    <div class="row">
        <div class="col-md-4 col-md-offset-8">
            <button class="btn btn-success" id="add_resume">新增</button>
        </div>
    </div>

    <!-- 简历数据在表格中显示 -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>姓名</th>
                    <th>地址</th>
                    <th>操作</th>
                </tr>
                <%
                    List<Resume> resumeList = (List<Resume>) request.getAttribute("resumes");
                    for (Resume resume : resumeList) {
                        String href = "update?name=" + resume.getName() + "&address=" + resume.getAddress() + "&phone=" + resume.getPhone() + "&id=" + resume.getId();
                %>
                <tr>
                    <th><%=resume.getId() %>
                    </th>
                    <th><%=resume.getName() %>
                    </th>
                    <th><%=resume.getAddress() %>
                    </th>
                    <th><%=resume.getPhone() %>
                    </th>
                    <th>
                        <button class="btn btn-primary btn-sm">
                            <a type="button" methods="get" class="editBtn btn btn-xs btn-success" id="modify_resume"
                               href="<%=href %>">编辑</a>
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button class="btn btn-danger btn-sm">
                            <a type="button" methods="get" class="delBtn btn btn-xs btn-success" href="./delete?id=<%=resume.getId() %>">删除</a>
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </button>
                    </th>
                </tr>
                <%
                    }
                %>
            </table>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#add_resume").click(function () {
            location.href = "add";
        });
        $("#modify_resume").click(function () {
            location.href = "add";
        });
    });
    function modify_resume() {
        location.href = 'list?name=' + $('#resume_name').val() + '&address=' + $('#resume_name').val() + '&phone=' + $('#resume_name').val();
    }
</script>
</body>
</html>
