package com.lagou.edu.service;

import com.lagou.edu.dao.ResumeDao;
import com.lagou.edu.pojo.Resume;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ResumeService {

    @Resource
    private ResumeDao resumeDao;

    public void delete(Long id) {
        resumeDao.deleteById(id);
    }

    public List<Resume> findAll() {
        return resumeDao.findAll();
    }

    public Long save(Resume resume) {
        resumeDao.save(resume);
        return resume.getId();
    }

}
