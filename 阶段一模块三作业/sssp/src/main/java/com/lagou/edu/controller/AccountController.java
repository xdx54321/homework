package com.lagou.edu.controller;

import com.lagou.edu.dao.LoginHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/login")
public class AccountController {

    @RequestMapping("/check")
    public String login(String username, String password, HttpServletRequest request) {

        if ("admin".equals(username) && "admin".equals(password)) {
            LoginHelper.addLoginUsername(username);
            request.getSession().setAttribute("username", username);
            return "redirect:/resume/list";
        }
        return "redirect:../login";
    }

    @RequestMapping("")
    public ModelAndView login() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

}
