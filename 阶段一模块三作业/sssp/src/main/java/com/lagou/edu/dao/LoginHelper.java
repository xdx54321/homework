package com.lagou.edu.dao;

import java.util.HashSet;
import java.util.Set;

public class LoginHelper {

    private static Set<String> loginUserSet = new HashSet<>();

    public static void addLoginUsername(String username) {
        loginUserSet.add(username);
    }

    public static boolean hasLogin(String username) {
        return loginUserSet.contains(username);
    }

}
