package com.lagou.service;

import com.lagou.dao.LagouUserDao;
import com.lagou.entity.LagouUser;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LoginService {

    @Resource
    private LagouUserDao lagouUserDao;

    public String getPassword(String username) {
        return lagouUserDao.getPassword(username);
    }

}
