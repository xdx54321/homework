package com.lagou.dao;

import com.lagou.entity.LagouUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

public interface LagouUserDao extends Mapper<LagouUser> {

    @Select("select password from lagou_user where username = #{username}")
    String getPassword(@Param("username") String username);

}
