
# 演示视频

链接: https://pan.baidu.com/s/1IJe9IgPJhqYnfexwAKNj5g  密码: 9okt

# 建表SQL
```sql
drop table if exists `t_hot_post`;
create table t_hot_post
(
	id bigint auto_increment comment '主键',
	post_name varchar(64) not null comment '职位名称',
	post_time datetime not null comment '发布时间',
	salary varchar(32) null comment '薪资说明',
	experience varchar(32) null comment '经验要求',
	education varchar(16) null comment '学历说明',
	tags varchar(128) null comment '职位标签,以英文逗号分隔。',
	company varchar(32) null comment '公司名称',
	logo varchar(256) null comment '公司logo',
	location varchar(16) null comment '公司所在地',
	cat varchar(16) null comment '公司类别',
	industry_tag varchar(128) null comment '行业标签,以英文逗号分隔',
	constraint t_hot_post_pk
		primary key (id)
)
comment '热门职位表';
```