package com.lagou.job.service;

import com.alibaba.fastjson.JSON;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.lagou.job.bean.HotPost;
import com.lagou.job.dao.HotPostDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class PostService {

    private String CACHE_KEY = "lagou:hot_post";

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private HotPostDao hotPostDao;

    LoadingCache<String, List<HotPost>> cache = CacheBuilder.newBuilder()
            .expireAfterAccess(10, TimeUnit.SECONDS)
            .build(new CacheLoader<String, List<HotPost>>() {
                        @Override
                        public List<HotPost> load(String key) throws Exception {
                            // 从缓存取
                            log.info("本地缓存中没有热门职位数据, 从redis中获取");
                            String value = stringRedisTemplate.opsForValue().get(CACHE_KEY);
                            if (value != null) {
                                log.info("redis中找到热门职位列表");
                                return JSON.parseArray(value, HotPost.class);
                            }
                            log.info("redis中找不到数据, 改从DB中获取");
                            List<HotPost> hotPosts = hotPostDao.findAll();
                            stringRedisTemplate.opsForValue().set(CACHE_KEY, JSON.toJSONString(hotPosts), 60, TimeUnit.SECONDS);
                            return hotPosts;
                        }
                    });

    public PostService() {
    }

    public List<HotPost> getHotPostList() {
        try {
            return cache.get(CACHE_KEY);
        } catch (ExecutionException e) {
            return new LinkedList<>();
        }
    }

}
