package com.lagou.job.controller;

import com.lagou.job.bean.HotPost;
import com.lagou.job.service.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/post")
@Slf4j
public class PostController {

    @Resource
    private PostService postService;

    /**
     * 获取热门职位
     *
     * @return
     */
    @GetMapping("/hot")
    @ResponseBody
    public List<HotPost> getHotPostList() {
        log.info("获取热门职位");
        return postService.getHotPostList();
    }

}
