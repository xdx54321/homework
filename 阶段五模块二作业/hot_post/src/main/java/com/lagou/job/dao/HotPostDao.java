package com.lagou.job.dao;

import com.lagou.job.bean.HotPost;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HotPostDao extends JpaRepository<HotPost, Long> {
}
