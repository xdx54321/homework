package com.lagou.job.bean;

import lombok.Data;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "t_hot_post")
@Proxy(lazy = false)
public class HotPost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 职位名称
     */
    @Column(name = "post_name")
    private String postName;

    /**
     * 发布时间
     */
    @Column(name = "post_time")
    private Date postTime;

    /**
     * 薪资要求
     */
    private String salary;

    /**
     * 经验要求
     */
    private String experience;

    /**
     * 学历要求
     */
    private String education;

    /**
     * 职位标签
     */
    private String tags;

    /**
     * 公司名称
     */
    private String company;

    /**
     * 公司logo
     */
    private String logo;

    /**
     * 公司所在地
     */
    private String location;

    /**
     * 行业标签
     */
    @Column(name = "industry_tag")
    private String industryTag;

    /**
     * 公司类型
     */
    private String cat;

}
