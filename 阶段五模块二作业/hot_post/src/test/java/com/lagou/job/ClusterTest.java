package com.lagou.job;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClusterTest {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void testString (){
        stringRedisTemplate.opsForValue().set("name", "xdx");
        System.out.println(stringRedisTemplate.opsForValue().get("name"));
    }

}
