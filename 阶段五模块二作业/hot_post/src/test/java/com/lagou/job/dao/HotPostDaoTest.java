package com.lagou.job.dao;

import com.lagou.job.bean.HotPost;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HotPostDaoTest {

    @Resource
    private HotPostDao hotPostDao;

    @Test
    public void testAdd() {
        String[] posts = new String[] {"C++工程师", "Java工程师", "PHP工程师", "Python工程师", "产品经理", "运营", "CTO", "Ruby工程师"};
        String[] salary = new String[] {"面议", "9-15K", "25-36K", "20-30K"};
        String[] cities = new String[] {"广州", "深圳", "上海", "北京", "杭州"};
        String[] industryTags = new String[] {"互联网", "社交", "餐饮", "电商"};
        String[] companies = new String[] {"Lagou", "Shopee", "Alibaba", "Tencent", "Baidu", "网易"};
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 0; i < 10; i++) {
            HotPost hotPost = new HotPost();
            hotPost.setPostName(posts[random.nextInt(0, posts.length - 1)]);
            hotPost.setEducation("不限");
            hotPost.setSalary(salary[random.nextInt(0, salary.length - 1)]);
            hotPost.setPostTime(new Date());
            hotPost.setExperience("3-5年");
            hotPost.setTags("开发,互联网");
            hotPost.setCompany(companies[random.nextInt(0, companies.length - 1)]);
            hotPost.setLogo("https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1816948731,4203604980&fm=26&gp=0.jpg");
            hotPost.setIndustryTag(industryTags[random.nextInt(0, industryTags.length - 1)]);
            hotPost.setLocation(cities[random.nextInt(0, cities.length - 1)]);
            hotPost.setCat("上市公司");
            hotPostDao.save(hotPost);
        }
    }

    @Test
    public void testGetSet() {

        HotPost hotPost = new HotPost();
        hotPost.setPostName("Java工程师");
        hotPost.setEducation("不限");
        hotPost.setSalary("面议");
        hotPost.setPostTime(new Date());
        hotPost.setExperience("3-5年");
        hotPost.setTags("开发,互联网,JAVA");
        hotPostDao.save(hotPost);

        HotPost post = hotPostDao.getOne(hotPost.getId());
        Assert.assertNotNull(post);
        Assert.assertEquals(hotPost.getEducation(), post.getEducation());
        Assert.assertEquals(hotPost.getExperience(), post.getExperience());
        Assert.assertEquals(hotPost.getPostName(), post.getPostName());
        Assert.assertEquals(hotPost.getTags(), post.getTags());

        hotPostDao.deleteById(hotPost.getId());
    }

}
