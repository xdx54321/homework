package com.dubbo.provider.a.server.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.dubbo.provider.a.api.HelloService;

import java.util.concurrent.ThreadLocalRandom;

@Service
public class HelloServiceImpl implements HelloService {

    private ThreadLocalRandom random = ThreadLocalRandom.current();

    @Override
    public String sayHello() {
        return "Response By A: Hello!";
    }

    @Override
    public void methodA() {
        try {
            Thread.sleep(random.nextLong(0, 100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void methodB() {
        try {
            Thread.sleep(random.nextLong(0, 100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void methodC() {
        try {
            Thread.sleep(random.nextLong(0, 100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
