package com.dubbo.provider.a.api;

public interface HelloService {

    String sayHello();

    void methodA();

    void methodB();

    void methodC();

}
