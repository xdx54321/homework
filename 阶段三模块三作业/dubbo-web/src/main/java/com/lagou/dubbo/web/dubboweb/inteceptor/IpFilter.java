package com.lagou.dubbo.web.dubboweb.inteceptor;

import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;
import com.lagou.dubbo.web.dubboweb.service.MonitorPrinter;
import com.lagou.dubbo.web.dubboweb.utils.ThreadLocalUtils;
import lombok.extern.slf4j.Slf4j;

@Activate
@Slf4j
public class IpFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        RpcContext.getContext().setAttachment("clientIP", ThreadLocalUtils.get("clientIP"));
        long begin = System.currentTimeMillis();
        Result result;
        try {
            result = invoker.invoke(invocation);
        } finally {
            long duration = System.currentTimeMillis() - begin;
            MonitorPrinter.monitor(invocation.getMethodName(), (int) duration);
        }
        // 每隔5秒输出一次结果

        return result;
    }

}
