package com.lagou.dubbo.web.dubboweb.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dubbo.provider.a.api.ByeService;
import org.springframework.stereotype.Service;


@Service
public class ByeServiceImpl {

    @Reference
    private ByeService byeService;

    public String sayBye() {
        return byeService.sayBye();
    }

}
