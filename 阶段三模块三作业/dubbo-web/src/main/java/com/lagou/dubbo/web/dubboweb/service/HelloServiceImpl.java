package com.lagou.dubbo.web.dubboweb.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dubbo.provider.a.api.HelloService;
import org.springframework.stereotype.Service;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
public class HelloServiceImpl {

    @Reference
    private HelloService helloService;

    public String sayHello() {
        return helloService.sayHello();
    }

    public void startTest() throws InterruptedException {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(32, 2000, 60000L,
                TimeUnit.MICROSECONDS, new ArrayBlockingQueue<>(100000));
        while (true) {
            executor.execute(() -> helloService.methodA());
            executor.execute(() -> helloService.methodB());
            executor.execute(() -> helloService.methodC());
            Thread.sleep(10L);
        }

    }

}
