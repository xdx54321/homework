package com.lagou.dubbo.web.dubboweb;

import com.lagou.dubbo.web.dubboweb.service.MonitorPrinter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DubboWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboWebApplication.class, args);
        new MonitorPrinter().start();
    }

}
