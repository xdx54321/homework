package com.lagou.dubbo.web.dubboweb.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class MonitorPrinter extends Thread {

    private static Map<String, int[]> RECORD = new ConcurrentHashMap<>();

    public static void monitor(String methodName, int duration) {
        // 用一个数组
        synchronized (methodName) {
            if (!RECORD.containsKey(methodName)) {
                RECORD.put(methodName, new int[100]);
            }
        }
        // 60秒分成100份，每份600毫秒, 当前时间在哪个600毫秒内呢
        RECORD.get(methodName)[(int) (System.currentTimeMillis() % 600 % 6)] = duration;
    }

    @SneakyThrows
    @Override
    public void run() {
        while (true) {
            Thread.sleep(5000L);
            for (Map.Entry<String, int[]> stringEntry : RECORD.entrySet()) {
                log.info("方法{}的TP90: {}, TP99: {}", stringEntry.getKey(),
                        tp(stringEntry.getValue(), 90), tp(stringEntry.getValue(), 99));
            }
        }
    }

    private static int tp(int[] times, int percent) {
        float percentF = (float) percent / 100;
        int index = (int) (percentF * times.length - 1);
        Arrays.sort(times);
        return times[index];
    }

}
