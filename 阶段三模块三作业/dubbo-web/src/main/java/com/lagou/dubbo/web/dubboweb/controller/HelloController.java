package com.lagou.dubbo.web.dubboweb.controller;

import com.dubbo.provider.a.api.ByeService;
import com.lagou.dubbo.web.dubboweb.service.ByeServiceImpl;
import com.lagou.dubbo.web.dubboweb.service.HelloServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class HelloController {

    @Resource
    private HelloServiceImpl helloServiceImpl;

    @Resource
    private ByeServiceImpl byeService;

    @GetMapping("/hello")
    public String sayHello() {
        return helloServiceImpl.sayHello() + "\n" + byeService.sayBye();
    }

    @GetMapping("/beginTest")
    public String beginTest() {
        new Thread(() -> {
            try {
                helloServiceImpl.startTest();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        return "start success";
    }

}
