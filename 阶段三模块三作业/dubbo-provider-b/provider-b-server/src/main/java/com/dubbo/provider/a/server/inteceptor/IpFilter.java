package com.dubbo.provider.a.server.inteceptor;

import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;
import lombok.extern.slf4j.Slf4j;

@Activate
@Slf4j
public class IpFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        String clientIP = RpcContext.getContext().getAttachment("clientIP");
        log.info("客户端IP: {}", clientIP);
        return invoker.invoke(invocation);
    }

}
