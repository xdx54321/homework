package com.dubbo.provider.a.server.service;

import com.alibaba.dubbo.config.annotation.Service;

@Service
public class HelloServiceImpl implements com.dubbo.provider.a.api.ByeService {


    @Override
    public String sayBye() {
        return "Response: Say Bye By B";
    }
}
