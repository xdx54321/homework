package com.dubbo.provider.a.api;

public interface ByeService {

    String sayBye();

}
