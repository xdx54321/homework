# 演示视频

## Redis集群演示

链接: https://pan.baidu.com/s/1_gmWA5wE6vJ0nKJHqNd3KA  密码: 40f9

## JCluster操作演示

# 安装Redis集群

## 规划
三主三从
端口从9001 - 9006

## 安装步骤
### 准备redis安装环境
下载安装包并解压
```
wget http://download.redis.io/releases/redis-5.0.9.tar.gz
tar xvf redis-5.0.9.tar.gz
```
分别执行下面命令来编译安装
```
cd redis-5.0.9
make
cd src
make install
```
创建相关目录
```
mkdir redis-cluster  # 集群目录, 集群所有节点的数据都在该目录下
cd redis-cluster
mkdir -p 9001/data 9002/data 9003/data 9004/data 9005/data 9006/data  # 每个目录表示一个节点
```
 
### 复制出一个实例
复制redis相关文件到每个节点目录下, 以9001为例, 其余节点操作类似
```
cp -r /opt/redis/redis-5.0.9/* /opt/redis-cluster/9001

```
修改redis.conf
```
port 9001
daemonize yes
dir /opt/redis-cluster/9001/data/
cluster-enabled yes
protected-mode no
# 下面的配置要注释掉
# bind 127.0.0.1  
```

### 复制其余实例
把上面的实例复制到9002-9006目录中
```
cp -rf /opt/redis-cluster/9001/* /opt/redis-cluster/9002
cp -rf /opt/redis-cluster/9001/* /opt/redis-cluster/9003
cp -rf /opt/redis-cluster/9001/* /opt/redis-cluster/9004
cp -rf /opt/redis-cluster/9001/* /opt/redis-cluster/9005
cp -rf /opt/redis-cluster/9001/* /opt/redis-cluster/9006
```
修改 9002-9006 的 redis.conf 文件
```
vim redis.conf
:%s/9001/9002/g  # 直接用全局替换命令, 该处以9002为例。其余目录分别改为9003、9004、9005、9006
```

### 启动Redis集群
在redis-cluster目录下新建 start-cluster.sh
```
/opt/redis-cluster/9001/src/redis-server /opt/redis-cluster/9001/redis.conf 
/opt/redis-cluster/9002/src/redis-server /opt/redis-cluster/9002/redis.conf 
/opt/redis-cluster/9003/src/redis-server /opt/redis-cluster/9003/redis.conf 
/opt/redis-cluster/9004/src/redis-server /opt/redis-cluster/9004/redis.conf 
/opt/redis-cluster/9005/src/redis-server /opt/redis-cluster/9005/redis.conf 
/opt/redis-cluster/9006/src/redis-server /opt/redis-cluster/9006/redis.conf
```
修改完成后用 ```chmod +x start-cluster.sh ``` 赋予执行权限
然后执行 ``` ./start-cluster.sh ``` 启动

创建Redis集群
```
# cluster-replicas ： 1     1从机 前三个为主
redis-cli --cluster create 192.168.3.39:9001 192.168.3.39:9003 192.168.3.39:9005 192.168.3.39:9002 192.168.3.39:9004 192.168.3.39:9006 --cluster-replicas 1
```

### 测试
连接任意一个节点
```
redis-cli -h 192.168.3.39 -p 9001 -c
```
查看集群状态
```
cluster info 
cluster nodes
```
测试读写数据
```
set xdxxdx a
get xdxxdx
```

### 添加一主一从
创建新节点相关文件
```
mkdir -p /opt/redis-cluster/9007/data /opt/redis-cluster/9008/data
cp -rf /opt/redis-cluster/9001/* /opt/redis-cluster/9007
cp -rf /opt/redis-cluster/9001/* /opt/redis-cluster/9008
```
分别修改配置
```
vim redis.conf
:%s/9001/9007/g  
```
启动节点
```
/opt/redis-cluster/9007/src/redis-server /opt/redis-cluster/9007/redis.conf 
/opt/redis-cluster/9008/src/redis-server /opt/redis-cluster/9008/redis.conf
```

将新结点作为集群新结点
```
redis-cli --cluster add-node 192.168.3.39:9007 192.168.3.39:9001
redis-cli -h 192.168.3.39 -p 9001 -c
```
对新节点的主节点分配槽
```
# 连接上任意一个可用节点
redis-cli --cluster reshard 192.168.3.39:9001
# 输入要分配的槽数量
3000
# 输入接收槽的结点id， cluster nodes命令返回新节点的id
d7d66c269329eac514c6edba8d88f5492f0ef977
# 输入源结点id  
all
# 输入yes开始移动槽到目标结点id
yes
```
添加从节点
```
#redis-cli --cluster add-node 新节点的ip和端口 旧节点ip和端口 --cluster-slave --cluster-master-id 主节点id
redis-cli --cluster add-node 192.168.3.39:9008 192.168.3.39:9007 --cluster-slave --cluster-master-id d7d66c269329eac514c6edba8d88f5492f0ef977
#检查结果
redis-cli -h 192.168.3.39 -p 9001 -c
cluster nodes
```

# 测试代码

代码详见 jedis_demo 目录

## 运行单元测试
```
JedisClusterTest.testKey()
```


