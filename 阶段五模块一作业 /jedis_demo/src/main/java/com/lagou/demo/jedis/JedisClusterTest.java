package com.lagou.demo.jedis;

import org.junit.Test;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class JedisClusterTest {

    private static JedisCluster jedis;

    static {
        // 添加集群的服务节点Set集合
        Set<HostAndPort> hostAndPortsSet = new HashSet<HostAndPort>();
        // 添加节点
        hostAndPortsSet.add(new HostAndPort("192.168.3.39", 9001));
        hostAndPortsSet.add(new HostAndPort("192.168.3.39", 9002));
        hostAndPortsSet.add(new HostAndPort("192.168.3.39", 9003));
        hostAndPortsSet.add(new HostAndPort("192.168.3.39", 9004));
        hostAndPortsSet.add(new HostAndPort("192.168.3.39", 9005));
        hostAndPortsSet.add(new HostAndPort("192.168.3.39", 9006));

        // Jedis连接池配置
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        // 最大空闲连接数, 默认8个
        jedisPoolConfig.setMaxIdle(100);
        // 最大连接数, 默认8个
        jedisPoolConfig.setMaxTotal(500);
        //最小空闲连接数, 默认0
        jedisPoolConfig.setMinIdle(0);
        // 获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认-1
        jedisPoolConfig.setMaxWaitMillis(2000); // 设置2秒
        //对拿到的connection进行validateObject校验
        jedisPoolConfig.setTestOnBorrow(true);
        jedis = new JedisCluster(hostAndPortsSet, jedisPoolConfig);
    }

    /**
     * 测试key:value数据
     * 集群中flushDB、keys废弃
     */
    @Test
    public void testKey() {

        System.out.println("判断键username是否存在：" + jedis.exists("username"));
        System.out.println("新增<'username','xdx'>的键值对：" + jedis.set("username", "xdx"));
        System.out.println("是否存在:" + jedis.exists("username"));

        System.out.println("新增<'password','123456'>的键值对：" + jedis.set("password", "123456"));
        System.out.println("删除键password:" + jedis.del("password"));
        System.out.println("判断键password是否存在：" + jedis.exists("password"));

    }

}