# Solr作业

视频演示百度网盘链接: https://pan.baidu.com/s/1-i5DdeQdwmIED2WWGdvZMw  密码: 58bo

# Solr安装

## 环境说明

Linux & apache-tomcat-9.0.31 & MySQL

## 下载 & 解压安装包
```
wget https://mirror.bit.edu.cn/apache/lucene/solr/7.7.3/solr-7.7.3.tgz
tar -xvf solr-7.7.3.tgz
```
## 复制包到tomcat的webapps下
```
# 进入Solr的server目录下 
cd ~/solr-7.7.3/server/solr-webapp/ 
# 复制webapp目录到tomcat-solr的webapps目录下 
cp -r webapp/ /root/solr/apache-tomcat-8.0.52/webapps 
# 将Solr的web应用改名 
cd ~/apache-tomcat-8.0.52/webapps 
# 将webapp重命名为solr 
mv webapp solr
```
## 复制需要的jar包
复制solr-7.7.3\server\lib\ext下的jar包到apache-tomcat-9.0.31\webapps\solr\WEB-INF\lib下，
以及复制solr-7.7.3\server\lib下所有metrics-开头的jar包（一共有5个）
```
cp ext/* ~/apache-tomcat-8.0.52/webapps/solr/WEB-INF/lib/ 
cp metrics-* ~/apache-tomcat-8.0.52/webapps/solr/WEB-INF/lib/
```

## 配置solrhome
```
mkdir -p ~/solr/solrhome
# 复制server/solr目录下所有内容到solrhome
cp -r * ~/solr/solrhome/
```

### 配置tomcat
修改复制到tomcat中的Solr项目中的 WEB-INF 目录下的 web.xml 配置文件
修改 solr_home 路径指向我们刚刚创建的 solrhome
```
<!-- 修改solrhome路径 --> 
<env-entry> 
	<env-entry-name>solr/home</env-entry-name> 
	<env-entry-value>/home/xdxa/solr/solrhome</env-entry-value> 
	<env-entry-type>java.lang.String</env-entry-type> 
</env-entry>
```
去掉tomcat安全认证： 注释掉 <security-constraint> 节点

配置solr_core
```
cp ~/solr-7.7.3/server/solr/configsets/_default/conf -rf ~/solr/solrhome/new_core
cd ~/solr/solrhome/new_core/conf
mv managed-schema schema.xml
```

### 启动tomcat
1. 进入tomcat目录执行
```
./bin/startup.sh
```
2. 在浏览器上访问solr

http://IP:端口/solr/index.html

3. 点击Core Admin 新建core_new


## 导入MySQL数据

### 执行作业给出的SQL（过程略）

### 复制DIH相关jar包
```
cp ~/solr-7.7.3/dist/solr-dataimporthandler-7.7.3.jar ~/solr-7.7.3/server/lib
cp ~/solr-7.7.3/dist/solr-dataimporthandler-extras-7.7.3.jar ~/solr-7.7.3/server/lib
```

### 找到solr7.7.3/example/example-DIH/solr/db/conf/db-data-config.xml，把其复制到
solrhome/new_core/conf/下，并改名为data-config.xml。
```
mv ~/solr7.7.3/example/example-DIH/solr/db/conf/db-data-config.xml ~/solr/solrhome/new_core/conf/data-config.xml
```
### 编辑data-config.xml，完整的配置文件如下
```
<dataConfig> 
	<!-- 这是mysql的配置 --> 
	<dataSource driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/lagou_position?useUnicode=true&amp;characterEncoding=utf8&amp;useSSL=false&amp;serverTimezone=GMT" user="mysql" password="mysql" /> 
	<document> 
		<!-- query是一条sql，代表在数据库查找出来的数据, 不建议用* -->
		<entity name="position" query="select id,companyName,positionAdvantage,companyId,positionName,salary,salaryMin,salaryMax,salaryMonth,education,workYear,jobNature,createTime,email from position"> 
			<!-- 每一个field映射着数据库中列与文档中的域，column是数据库列，name是 solr的域(必须是在managed-schema文件中配置过的域才行) --> 
			<field column="id" name="id"/> 
			<field column="companyName" name="companyName"/> 
			<field column="positionAdvantage" name="positionAdvantage"/> 
			<field column="companyId" name="companyId"/> 
			<field column="positionName" name="positionName"/> 
			<field column="salary" name="salary"/> 
			<field column="salaryMin" name="salaryMin"/> 
			<field column="salaryMax" name="salaryMax"/> 
			<field column="salaryMonth" name="salaryMonth"/> 
			<field column="education" name="education"/> 
			<field column="workYear" name="workYear"/> 
			<field column="jobNature" name="jobNature"/> 
			<field column="createTime" name="createTime"/> 
			<field column="email" name="email"/> 
		</entity>
	</document> 
</dataConfig>
```

### 打开solrconfig.xml，在里面添加一段内容，如下
```
<requestHandler name="/dataimport" class="org.apache.solr.handler.dataimport.DataImportHandler"> 
	<lst name="defaults"> 
		<str name="config">data-config.xml</str> 
	</lst> 
</requestHandler>
```

### 添加分词器
1. 复制jar包
```
cp ~/solr-7.7.3/contrib/analysis-extras/lucene-libs/lucene-analyzers-smartcn-7.7.3.jar ~/apache-tomcat-9.0.31/webapps/solr/WEB-INF/lib/ 
```
2. 修改managed-schema, 新增
```
<!--solr cnAnalyzer-->
  <fieldType name="solr_cnAnalyzer" class="solr.TextField" positionIncrementGap="100">
      <analyzer type="index">
        <tokenizer class="org.apache.lucene.analysis.cn.smart.HMMChineseTokenizerFactory"/>
      </analyzer>
      <analyzer type="query">
        <tokenizer class="org.apache.lucene.analysis.cn.smart.HMMChineseTokenizerFactory"/>
      </analyzer>
  </fieldType>
```

### 在new_core/conf/managed-schema 配置对应的域
```
<field name="companyName" type="solr_cnAnalyzer" indexed="true" stored="true"/> 
<field name="positionAdvantage" type="solr_cnAnalyzer" indexed="true" stored="true"/> 
<field name="companyId" type="plong" indexed="true" stored="true"/> 
<field name="positionName" type="solr_cnAnalyzer" indexed="true" stored="false" />

<field name="salary" type="string" indexed="false" stored="true"/> 
<field name="salaryMin" type="string" indexed="true" stored="true"/> 
<field name="salaryMax" type="string" indexed="true" stored="true"/> 
<field name="salaryMonth" type="string" indexed="true" stored="true"/> 
<field name="education" type="string" indexed="true" stored="true"/> 

<field name="workYear" type="string" indexed="true" stored="true"/> 
<field name="jobNature" type="string" indexed="true" stored="true"/> 
<field name="createTime" type="pdate" indexed="true" stored="true"/> 
<field name="email" type="string" indexed="false" stored="true"/> 
```

### 在控制台执行导入
```
~/apache-tomcat-9.0.31/bin/shutdown.sh
~/apache-tomcat-9.0.31/bin/startup.sh
```
访问solr控制台,找到new_core中的dataimport，点击蓝色的按钮，则开始导入。


