package com.lagou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ApplicationTest {
    public static void main(String[] args) {
        ApplicationContext  app = SpringApplication.run(ApplicationTest.class,args);
    }
}
