package com.lagou.bean;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.util.Date;

@Data
@SolrDocument(collection = "position")
public class Position {

    @Field
    @Id
    private String id;

    @Field
    private String companyName;

    @Field
    private String positionAdvantage;

    @Field
    private Long companyId;

    @Field
    private String positionName;

    @Field
    private String salary;

    @Field
    private String salaryMin;

    @Field
    private String salaryMax;

    @Field
    private String salaryMonth;

    @Field
    private String education;

    @Field
    private String workYear;

    @Field
    private String jobNature;

    @Field
    private Date createTime;

    @Field
    private String email;

}
