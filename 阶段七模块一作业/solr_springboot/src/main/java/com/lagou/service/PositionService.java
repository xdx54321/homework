package com.lagou.service;

import com.lagou.bean.Position;
import lombok.extern.slf4j.Slf4j;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

@Service
@Slf4j
public class PositionService {

    @Resource
    private SolrClient solrClient;

    private static final int LIMIT = 5;

    public List<Position> list(String query) {

        SolrQuery solrQuery = new SolrQuery();
        solrQuery.set("df", "positionName");
        solrQuery.setQuery(query);
        solrQuery.setStart(0);
        solrQuery.setRows(LIMIT);
        try {
            QueryResponse queryResponse = solrClient.query(solrQuery);
            if (queryResponse == null){
                return  null;
            }
            // 获取列表
            List<Position> positions = queryResponse.getBeans(Position.class);
            if (positions.size() < 5) {
                // 不满5条, 继续查询
                log.info("查询结果不到5条, 继续补充: {}", query);
                SolrQuery supQuery = new SolrQuery();
                supQuery.set("df", "positionAdvantage");
                supQuery.setQuery("美女多");
                supQuery.setQuery("员工福利好");
                supQuery.setStart(0);
                supQuery.setRows(LIMIT - positions.size());
                QueryResponse supResponse = solrClient.query(supQuery);
                if (supResponse != null) {
                    positions.addAll(supResponse.getBeans(Position.class));
                }
            }
            return  positions;
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }

}
