
# 演示视频

链接: https://pan.baidu.com/s/1UKKjr48xeIfcQ4XN294L2A  密码: m8mu

# 作业要求

基于RabbitMQ的TTL以及死信队列，使用SpringBoot实现延迟付款，手动补偿操作。
1、用户下单后展示等待付款页面
2、在页面上点击付款的按钮，如果不超时，则跳转到付款成功页面
3、如果超时，则跳转到用户历史账单中查看因付款超时而取消的订单。

# 环境搭建步骤
1. 拉取RabbitMQ镜像
```
docker pull rabbitmq:3.7.14-management
```
2.创建容器
```
docker run -d --name rabbitmq -p 5672:5672 -p 15672:15672 -v `$pwd`/data:/var/lib/rabbitmq --hostname myrabbit -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin rabbitmq:3.7.14-management
```
3. 检查是否启动
```
http://192.168.3.39:15672
```

# 代码

## 表结构
```sql
create table t_order
(
	id bigint auto_increment comment '主键',
	produce varchar(128) not null comment '商品名称',
	status int not null comment '订单状态,0-未支付,1-已支付,2-超时取消',
	constraint t_order_pk
		primary key (id)
)
comment '订单表';

```









