<%@ page import="java.util.List" %>
<%@ page import="com.lagou.edu.pojo.Order" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>订单列表</title>
    <!-- 找到web项目的webapp路径 -->
    <%
        pageContext.setAttribute("APP_PATH", request.getContextPath());
    %>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</head>
<body>
<!-- 搭建显示页面 -->
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <tr>
                    <th>订单编号</th>
                    <th>商品名称</th>
                    <th>状态</th>
                </tr>
                <%
                    List<Order> orders = (List<Order>) request.getAttribute("orders");
                    for (Order order : orders) {
                %>
                <tr>
                    <th><%=order.getId() %>
                    </th>
                    <th><%=order.getProduce() %>
                    </th>
                    <th><%=order.getStatus() == 0 ? "未支付" : order.getStatus() == 1 ? "已支付" : "超时取消"%>
                    </th>
                </tr>
                <%
                    }
                %>
            </table>
        </div>
    </div>

</div>
</body>
</html>
