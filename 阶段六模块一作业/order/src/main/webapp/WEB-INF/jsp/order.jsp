<%@page contentType="text/html;charset=utf-8" language="java" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>login test</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="ajax方式">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
<div id="form-div">
    <form id="form1" action="./submit" method="post">
        <label for="produce">商品名称</label>
        <input type="text" class="form-control" id="produce"
               placeholder="JAVA训练营" value="JAVA训练营" disabled="true"/>
        <input type="hidden" name="produce" value="JAVA训练营">
        <br/>
        <p><button type="submit" class="btn btn-default">提交</button></p>
    </form>
</div>
</body>
</html>