<%@page contentType="text/html;charset=utf-8" language="java" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>等待付款</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="ajax方式">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript">
        function pay() {
            var url = location.search; //获取url中"?"符后的字串 ('?modFlag=business&role=1')
            var theRequest = new Object();
            if ( url.indexOf( "?" ) != -1 ) {
                var str = url.substr( 1 ); //substr()方法返回从参数值开始到结束的字符串；
                var strs = str.split( "&" );
                for ( var i = 0; i < strs.length; i++ ) {
                    theRequest[strs[i].split("=")[0]] = (strs[i].split("=")[1]);
                }
                console.log(theRequest); //此时的theRequest就是我们需要的参数；
            }
            $.ajax({
                //几个参数需要注意一下
                type: "GET",//方法类型
                url: "http://127.0.0.1/order/pay?orderId=" + <%=request.getAttribute("orderId")%> ,
                success: function (result) {
                    if (result == -1) {
                        window.location.href="cancel.html";
                        return;
                    }
                    alert('付款成功');
                }
            });
        }


    </script>
</head>
<body>
<div id="form-div">
    <form id="form1" action="./pay" method="post">
        <label for="produce">商品名称</label>
        <input type="text" class="form-control" id="produce"
               value="<%=request.getAttribute("produce")%>" disabled="true"/>
        <input type="hidden" name="orderId" value="<%=request.getAttribute("orderId")%>">
        <p>正在等待付款......</p>
        <br/>
        <p><button type="submit" class="btn btn-default">付款</button></p>
    </form>
</div>
</body>
</html>