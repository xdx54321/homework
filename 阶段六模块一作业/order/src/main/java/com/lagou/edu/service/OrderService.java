package com.lagou.edu.service;

import com.lagou.edu.config.RabbitConfig;
import com.lagou.edu.dao.OrderDao;
import com.lagou.edu.pojo.Order;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class OrderService {

    private final int PAY_SUCCESS = 1;

    private final int UN_PAY = 0;

    private final int CANCEL = 2;

    @Resource
    private OrderDao orderDao;

    @Resource
    private AmqpTemplate rabbitTemplate;

    @RabbitListener(queues = RabbitConfig.Q_DLX)
    @Transactional(rollbackOn = Throwable.class)
    public void service(String message) {
        log.info("死信队列推送来的消息: {}", message);
        Long orderId = Long.parseLong(message);
        Order order = orderDao.getOne(orderId);
        if (order.getId() == null) {
            log.warn("找不到订单: {}", orderId);
            return;
        }
        if (order.getStatus() == 1) {
            log.info("订单已支付成功: {}", order);
            return;
        }
        order.setStatus(CANCEL);
        orderDao.save(order);
    }

    public Long order(Order order) {
        order.setStatus(0);
        orderDao.save(order);
        // 发送TTL消息到MQ
        MessageProperties properties = new MessageProperties();
        Message message = new Message(order.getId().toString().getBytes(StandardCharsets.UTF_8), properties);
        rabbitTemplate.convertAndSend(RabbitConfig.EX_PAY, "pay.waiting", message);
        return order.getId();
    }

    /**
     *
     * @param orderId
     * @return 1:支付成功、2:订单已取消、-1:订单不存在
     */
    public Integer pay(Long orderId) {
        Order order = orderDao.getOne(orderId);
        if (order.getStatus() == null) {
            return -1;
        }
        if (order.getStatus() == 0) {
            order.setStatus(1);
            orderDao.save(order);
            return 1;
        }
        return order.getStatus();
    }

    public List<Order> orderList(Integer status) {
        Specification<Order> query = (Specification<Order>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("status"), status));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        return orderDao.findAll(query);
    }

}
