package com.lagou.edu.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitConfig {

    public static final String EX_DLX = "ex.go.dlx";

    public static final String EX_PAY = "ex.pay.waiting";

    public static final String Q_DLX = "q.go.dlx";

    @Bean
    public Queue queueWaiting() {
        Map<String, Object> props = new HashMap<>();
        props.put("x-message-ttl", 5000);
        props.put("x-dead-letter-exchange", EX_DLX);
        props.put("x-dead-letter-routing-key", "go.dlx");
        Queue queue = new Queue("q.pay.waiting", false, false, false, props);
        return queue;
    }

    /**
     * 该交换器使用的时候，需要给每个消息设置有效期
     * @return
     */
    @Bean
    public Exchange exchangeWaiting() {
        Map<String, Object> props = new HashMap<>();
        DirectExchange exchange = new DirectExchange(EX_PAY, false, false, props);
        return exchange;
    }

    /**
     * 绑定TTL消息队列
     * @return
     */
    @Bean
    public Binding bindingWaiting() {
        return BindingBuilder.bind(queueWaiting()).to(exchangeWaiting()).with("pay.waiting").noargs();
    }




    /**
     * 死信交换机绑定死信队列
     * @return
     */
    @Bean
    public Binding bindingDlx() {
        return BindingBuilder.bind(queueDlx()).to(exchangeDlx()).with("go.dlx").noargs();
    }

    /**
     * 死信交换器
     * @return
     */
    @Bean
    public Exchange exchangeDlx() {
        DirectExchange exchange = new DirectExchange(EX_DLX, true, false, null);
        return exchange;
    }

    /**
     * 死信队列
     * @return
     */
    @Bean
    public Queue queueDlx() {
        Queue queue = new Queue(Q_DLX, true, false, false);
        return queue;
    }

}
