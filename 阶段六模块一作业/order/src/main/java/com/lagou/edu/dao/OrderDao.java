package com.lagou.edu.dao;

import com.lagou.edu.pojo.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OrderDao extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order>  {
}
