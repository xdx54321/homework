package com.lagou.edu.controller;

import com.lagou.edu.pojo.Order;
import com.lagou.edu.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/order")
@Slf4j
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("/add")
    public ModelAndView add() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("order");
        return modelAndView;
    }

    @PostMapping("/submit")
    public ModelAndView order(@RequestParam("produce") String produce, HttpServletRequest request) {
        Order order = new Order();
        order.setProduce(produce);
        log.info("新增订单: {}", order);
        Long orderId = orderService.order(order);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("payment");
        request.setAttribute("orderId", orderId);
        request.setAttribute("produce", produce);
        return modelAndView;
    }

    @PostMapping("/pay")
    public ModelAndView pay(@RequestParam("orderId") Long orderId, HttpServletRequest request) {
        Integer result = orderService.pay(orderId);
        ModelAndView modelAndView = new ModelAndView();
        if (result == 1) {
            modelAndView.setViewName("pay_success");
            return modelAndView;
        }
        request.setAttribute("orders", orderService.orderList(2));
        modelAndView.setViewName("history");
        return modelAndView;
    }

}
