package com.lagou.edu.pojo;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "t_order")
@Data
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String produce;

    private Integer status;

}
