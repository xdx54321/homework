package com.lagou.controller;

import com.lagou.bean.DeleteResult;
import com.lagou.bean.UpLoadResult;
import com.lagou.service.FileUpLoadService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping("/oss")
public class UploadController {

    @Autowired
    private FileUpLoadService  fileUpLoadService;

    @PostMapping("/upload")
    @ResponseBody
    public UpLoadResult upload(@RequestParam("file") MultipartFile  multipartFile){
        String fileName = multipartFile.getOriginalFilename();// jpg、png 或者 png
        if (!fileName.endsWith("jpg") && !fileName.endsWith("png") && !fileName.endsWith("png")) {
            UpLoadResult result = new UpLoadResult();
            result.setStatus("error");
            result.setResponse("文件扩展名不正确");
            return result;
        }
        return fileUpLoadService.upload(multipartFile);
    }

    @DeleteMapping("/delete")
    @ResponseBody
    public DeleteResult delete(@RequestParam("fileName") String fileName) {
        return fileUpLoadService.delete(fileName);
    }

    @GetMapping("/download")
    public void download(@RequestParam("fileName") String fileName, HttpServletRequest request, HttpServletResponse response) throws IOException {
        InputStream inputStream = fileUpLoadService.download(fileName);
        IOUtils.copy(inputStream, response.getOutputStream());
    }

}
