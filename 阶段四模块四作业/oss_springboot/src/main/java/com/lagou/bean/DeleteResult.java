package com.lagou.bean;

import lombok.Data;

@Data
public class DeleteResult {

    // 状态有：uploading done error removed
    private String status;
    // 服务端响应内容，如：'{"status": "success"}'
    private String response;
}
