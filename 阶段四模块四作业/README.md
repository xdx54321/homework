
# 作业一

## 运行效果录屏

链接: https://pan.baidu.com/s/1XUFiQA7ubHvw6UnfOG4VRQ  密码: 4meo

## 要求
```
业务场景：用户上传的头像图片大小不一、手机和PC等设备显示尺寸也存在差异，因此需要能根据http请求指定的尺寸参数对图片进行动态压缩，以达到屏幕适配的作用。
功能需求：搭建FastDFS图片服务器，通过http请求访问服务器中图片时，显示动态缩略图。
``` 

## 搭建环境
1. Linux
2. Docker

## 搭建FastDFS
### 下载fastdfs镜像
```
docker pull delron/fastdfs
```
### 开启tracker容器
```
docker run -d --restart=always --network=host -v /home/xdxa/fastdfs:/var/fdfs --name tracker delron/fastdfs tracker
```
### 开启Storage容器
```
docker run -d --restart=always --network=host --name storage -e TRACKER_SERVER=192.168.3.39:22122 -v /home/xdxa/fastdfs/storage:/var/fdfs -e GROUP_NAME=group1 delron/fastdfs storage
```
### 测试FastDFS功能
```
进入storage容器
docker exec -it storage /bin/bash

下载一张图片
wget https://img-blog.csdnimg.cn/2020050914073364.jpg

上传图片至FastDFS
/usr/bin/fdfs_upload_file /etc/fdfs/client.conf 2020050914073364.jpg 

通过浏览器来预览图片
http://192.168.3.39:8888/group1/M00/00/00/wKgDJ18yHCiASI6lAAFBiU9yeyQ716.jpg
```

## FastDFS集成Nginx+lua
### 准备依赖包
FastDFS整合Nginx+lua所需要的依赖包有
```
ngx_devel_kit
lua-nginx-module-0.10.9rc7
fastdfs-nginx-module-master
LuaJIT-2.0.4
ua-5.3.1
GraphicsMagick-1.3.18
fastdfs.lua
```
为方便使用将这些依赖文件上传至网盘, 下载地址：
链接: https://pan.baidu.com/s/1KMzHzXHxB9bkeFik8klAeA  密码: 9qkb
下载完成后将src.zip上传至storage容器下的/usr/local目录中备用
```
docker cp ./src.zip storage:/usr/local
```
### 安装软件基础包
```
# 进入storage容器
docker exec -it storage /bin/bash
yum install -y gcc gcc-c++ zlib zlib-devel openssl openssl-devel pcre pcre-devel gd-devel
yum install -y libpng libjpeg libpng-devel libjpeg-devel ghostscript libtiff libtiff-devel freetype freetype-devel readline-devel ncurses-devel
```

### 安装依赖
```
cd /usr/local
unzip src.zip
cd src

# 安装LuaJIT
cd /usr/local/src/LuaJIT-2.0.4
make && make install
export LUAJIT_LIB=/usr/local/lib
export LUAJIT_INC=/usr/local/include/luajit-2.0
ln -s /usr/local/lib/libluajit-5.1.so.2 /lib64/libluajit-5.1.so.2

# 安装Lua
cd /usr/local/src/lua-5.3.1
make linux && make install

# 安装GM
cd /usr/local/src/GraphicsMagick-1.3.18
./configure --prefix=/usr/local/GraphicsMagick-1.3.18 --enable-shared
make && make install
ln -s /usr/local/GraphicsMagick-1.3.18 /usr/local/GraphicsMagick

# 安装Nginx
# 停止Nginx服务
/usr/local/nginx/sbin/nginx -s stop
# 进入Nginx目录
cd /tmp/nginx/nginx-1.12.2
./configure --prefix=/usr/local/nginx \
--with-http_ssl_module \
--with-http_flv_module \
--with-http_stub_status_module \
--with-http_gzip_static_module \
--with-http_realip_module \
--add-module=/usr/local/src/fastdfs-nginx-module-master/src \
--with-pcre \
--add-module=/usr/local/src/lua-nginx-module-0.10.9rc7 \
--add-module=/usr/local/src/ngx_devel_kit \
--with-stream
make && make install
```

### 配置Lua脚本实现图片缩略图
fastdfs.lua的脚本第53行的IP需要修改为自己的tracker的IP
```
fdfs:set_tracker("192.168.3.39", 22122)
```

### 修改nginx.conf配置
编辑nginx.conf
```
vi /usr/local/nginx/conf/nginx.conf
```
修改内容如下
```
user  root;
server {
    listen 8888;
    server_name localhost;

    # LUA测试
    location /lua {
        default_type 'text/plain';
        content_by_lua 'ngx.say("hello,lua")';
    }

    # fastdfs 缩略图生成
    location ~/group[0-9]/ {
            alias /var/fdfs/data;
            # 缩略图保存目录，为fdfs保存文件的data目录的路径
            set $image_root "/var/fdfs/data";
            if ($uri ~ "/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/(.*)") {
              set $image_dir "$image_root/$3/$4/";
              set $image_name "$5";
              set $file "$image_dir$image_name";
            }

            if (!-f $file) {
              # 关闭lua代码缓存，方便调试lua脚本
              #lua_code_cache off;
              # 加载指定lua文件
              content_by_lua_file "/usr/local/src/lua/fastdfs.lua";
            }
            ngx_fastdfs_module;
    }
    # log file
    # access_log  logs/img_access.log access;
}
```
修改完成后重启nginx, 要注意
```
location ~/group[0-9]/ 
```
只能配一个, 否则nginx只会用最先出现的.

```
# 启动命令
/usr/local/nginx/sbin/nginx
# 查看日志
tail -f /usr/local/nginx/logs/error.log
```

### 测试缩略图
浏览器打开
```
http://192.168.3.39:8888/group1/M00/00/00/wKgAHl62RoSAM_OzALXOIv_8IdY923.jpg_110x110.jpg
```


# 作业二

## 运行效果录屏

链接: https://pan.baidu.com/s/1l5r55dOx-yeT_MJPQcQDbw  密码: 4sh5

## 要求
```
使用SpringBoot和OSS实现图片的上传、下载和删除功能.

提供上传接口 /oss/upload ，实现图片上传到OSS对应的Bucket中
类型检查：必须为 jpg、png 或者 jpeg类型，其它图片类型返回错误提示信息
大小检查：必须小于5M，大于5M时返回错误提示信息
图片名称：生成的文件名，必须保持唯一

提供下载请求接口 /oss/download，实现图片下载
可以根据图片名进行文件的下载

提供删除请求/oss/delete，实现图片删除
可以根据图片名进行文件的删除
```

## 详见代码目录 src


























