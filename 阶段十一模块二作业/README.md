# 作业内容

将模块一作业追加的课程功能进行容器化发布和Skywalking监控

# 演示视频

链接: https://pan.baidu.com/s/1tLwYmzDMedaE7qFYC0uiBg  密码: 62tg

# 操作步骤

## 部署Skywalking

### 部署服务端+UI
```
cd /opt/skywalking
wget https://mirror.bit.edu.cn/apache/skywalking/8.2.0/apache-skywalking-apm-es7-8.2.0.tar.gz# 创建并运行容器
tar xvf apache-skywalking-apm-es7-8.2.0.tar.gz
cd apache-skywalking-apm-bin-es7
./bin/startup.sh
```

## 部署课程模块

### 打包edu-course-boot-master项目
```
mvn clean package
```
### 创建Dockerfile
```
FROM ascdc/jdk8
VOLUME /tmp
ADD edu-course-boot.jar app.jar
ADD skywalking-agent.jar skywalking-agent.jar
ENTRYPOINT ["java","-javaagent:skywalking-agent.jar", "-Dskywalking.agent.service_name=edu-course-server", "-Dskywalking.collector.backend_service=192.168.1.200:11800","-jar","app.jar"]
```
### 生成镜像
```
docker build -t edu-course .
```
### 运行容器
```
docker run -d --net=host edu-course
```







