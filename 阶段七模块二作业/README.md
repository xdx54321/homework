# Elastic Search 作业

视频演示地址
链接: https://pan.baidu.com/s/1cOizOYioTYehKWpKqq8IDA  密码: l6hb

## 环境搭建
运行环境: Linux & Docker

### 拉取 & 启动
```
docker pull elasticsearch:7.2.0
docker run --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -d elasticsearch:7.2.0
```
### 验证
```
curl http://localhost:9200
# 本次返回
{
  "name" : "99243ce170cb",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "-3YvZPK-S0yow4v7Jy0apQ",
  "version" : {
    "number" : "7.2.0",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "508c38a",
    "build_date" : "2019-06-20T15:54:18.811730Z",
    "build_snapshot" : false,
    "lucene_version" : "8.0.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```
### 放开跨域
```
docker exec -it elasticsearch /bin/bash
cd /usr/share/elasticsearch/config/
vi elasticsearch.yml
# 在末尾加上下面内容
http.cors.enabled: true
http.cors.allow-origin: "*"
# 保存退出, 重启容器
exit
docker restart elasticsearch
```

### 安装中文分词器
```
docker exec -it elasticsearch /bin/bash
cd /usr/share/elasticsearch/plugins/
# 下载并安装插件
mkdir ik
cd ik
wget https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.2.0/elasticsearch-analysis-ik-7.2.0.zip
yum install unzip
unzip elasticsearch-analysis-ik-7.2.0.zip
exit
docker restart elasticsearch 
```
### 验证
```
curl -XGET -H 'Content-Type: application/json' 'http://localhost:9200/_analyze?pretty' -d '{
"analyzer" : "ik_max_word",
"text": "用于分词测试的句子"
}'
# 返回
{
  "tokens" : [
    {
      "token" : "用于",
      "start_offset" : 0,
      "end_offset" : 2,
      "type" : "CN_WORD",
      "position" : 0
    },
    {
      "token" : "分词",
      "start_offset" : 2,
      "end_offset" : 4,
      "type" : "CN_WORD",
      "position" : 1
    },
    {
      "token" : "测试",
      "start_offset" : 4,
      "end_offset" : 6,
      "type" : "CN_WORD",
      "position" : 2
    },
    {
      "token" : "的",
      "start_offset" : 6,
      "end_offset" : 7,
      "type" : "CN_CHAR",
      "position" : 3
    },
    {
      "token" : "句子",
      "start_offset" : 7,
      "end_offset" : 9,
      "type" : "CN_WORD",
      "position" : 4
    }
  ]
}
```

## 数据同步

启动项目, 执行下面命令
```
curl http://127.0.0.1:8083/importAll
```


