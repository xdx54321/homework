package com.lagou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MongoRepositoryMain {
    public static void main(String[] args) {
        SpringApplication.run(MongoRepositoryMain.class,args);
    }
}
