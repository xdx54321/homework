package com.lagou;

import com.lagou.bean.Resume;
import com.lagou.repository.ResumeRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MongoRepositoryMain.class)
public class ResumeRepositoryTest {

    @Resource
    private ResumeRepository resumeRepository;

    @Test
    public void testAddAndFind() {

        String name = "xdxtest";
        Resume resume  = new Resume();
        resume.setName(name);
        resume.setExpectSalary(99999);
        resume.setCity("sz");

        resumeRepository.save(resume);
        log.info("添加简历快照完成: {}", resume);
        List<Resume> resumeList = resumeRepository.findByNameEquals(name);
        log.info("姓名为{}的简历快照: {}", name, resumeList);
    }

}
