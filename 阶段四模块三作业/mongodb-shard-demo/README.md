# 演示视频
链接: https://pan.baidu.com/s/1t8Keem9CRqXwgTu8j-u9yA  密码: c9oo

# 1. 搭建集群
## 准备工作
### 节点规划
|  节点用途   | 节点IP:端口  | 
|  ----  | ----  |
| 路由节点  | 192.168.3.39:27017 |
| 配置节点1  | 192.168.3.39:17011 |
| 配置节点2 | 192.168.3.39:17013 |
| 配置节点3 | 192.168.3.39:17015 |
| 分片集群1节点1 | 192.168.3.39:37011 |
| 分片集群1节点2 | 192.168.3.39:37013 |
| 分片集群1节点3 | 192.168.3.39:37015 |
| 分片集群1节点4 | 192.168.3.39:37017 |
| 分片集群2节点1 | 192.168.3.39:47011 |
| 分片集群2节点2 | 192.168.3.39:47013 |
| 分片集群2节点3 | 192.168.3.39:47015 |
| 分片集群2节点4 | 192.168.3.39:47017 |
| 分片集群3节点1 | 192.168.3.39:57011 |
| 分片集群3节点2 | 192.168.3.39:57013 |
| 分片集群3节点3 | 192.168.3.39:57015 |
| 分片集群3节点4 | 192.168.3.39:57017 |
| 分片集群4节点1 | 192.168.3.39:58011 |
| 分片集群4节点2 | 192.168.3.39:58013 |
| 分片集群4节点3 | 192.168.3.39:58015 |
| 分片集群4节点4 | 192.168.3.39:58017 |
### 目录划分
|--mongo-cluster
   |   |--config
   |   |   |--config3
   |   |   |   |--logs
   |   |   |--config2
   |   |   |   |--logs
   |   |   |--config1
   |   |   |   |--logs
   |   |--shard
   |   |   |--shard4
   |   |   |   |--shard4-58017
   |   |   |   |--shard4-58013
   |   |   |   |--shard4-58015
   |   |   |   |--shard4-58011
   |   |   |--shard1
   |   |   |   |--shard1-37017
   |   |   |   |--shard1-37015
   |   |   |   |--logs
   |   |   |   |--shard1-37011
   |   |   |   |--shard1-37013
   |   |   |--shard2
   |   |   |   |--shard2-47017
   |   |   |   |--shard2-47011
   |   |   |   |--shard2-47013
   |   |   |   |--shard2-47015
   |   |   |--shard3
   |   |   |   |--shard3-57013
   |   |   |   |--shard3-57015
   |   |   |   |--shard3-57017
   |   |   |   |--shard3-57011
   |   |--bin
   |   |--route
   |   |   |--logs

## 搭建配置节点
### 节点1 config-17011.conf
```
# 数据库文件位置 
dbpath=config/config1 
#日志文件位置 
logpath=config/config1/logs/config1.log 
# 以追加方式写入日志 
logappend=true 
# 是否以守护进程方式运行 
fork=true 
bind_ip=0.0.0.0 
port=17011
# 表示是一个配置服务器 
configsvr=true 
#配置服务器副本集名称 
replSet=configsvr
```
### 节点2 config-17013.conf
```
# 数据库文件位置 
dbpath=config/config2
#日志文件位置 
logpath=config/config2/logs/config2.log 
# 以追加方式写入日志 
logappend=true 
# 是否以守护进程方式运行 
fork=true 
bind_ip=0.0.0.0 
port=17013
# 表示是一个配置服务器 
configsvr=true 
#配置服务器副本集名称 
replSet=configsvr
```
### 节点3 config-17015.conf
```
# 数据库文件位置 
dbpath=config/config3
#日志文件位置 
logpath=config/config3/logs/config3.log 
# 以追加方式写入日志 
logappend=true 
# 是否以守护进程方式运行 
fork=true 
bind_ip=0.0.0.0 
port=17015
# 表示是一个配置服务器 
configsvr=true 
#配置服务器副本集名称 
replSet=configsvr
```
### 启动
```
./bin/mongod -f config/config-17011.conf 
./bin/mongod -f config/config-17013.conf 
./bin/mongod -f config/config-17015.conf
```
### 添加集群配置
进入任一节点
```
./bin/mongo --port 17011
use admin 
var cfg = {"_id":"configsvr", "members":[ 
{"_id":1,"host":"192.168.3.39:17011"}, 
{"_id":2,"host":"192.168.3.39:17013"}, 
{"_id":3,"host":"192.168.3.39:17015"}] };
rs.initiate(cfg)
```

## 搭建分片节点
### shard1-37011节点
```
dbpath=shard/shard1/shard1-37011
bind_ip=0.0.0.0 
port=37011
fork=true 
logpath=shard/shard1/shard1-37011.log 
replSet=shard1 
shardsvr=true 
```
### shard1-37013节点
```
dbpath=shard/shard1/shard1-37013
bind_ip=0.0.0.0 
port=37013
fork=true 
logpath=shard/shard1/shard1-37013.log 
replSet=shard1 
shardsvr=true 
```
### shard1-37015节点
```
dbpath=shard/shard1/shard1-37015
bind_ip=0.0.0.0 
port=37015
fork=true 
logpath=shard/shard1/shard1-37015.log 
replSet=shard1 
shardsvr=true 
```
### shard1-37017节点
```
dbpath=shard/shard1/shard1-37017
bind_ip=0.0.0.0 
port=37017
fork=true 
logpath=shard/shard1/shard1-37017.log 
replSet=shard1 
shardsvr=true
```
### 启动
```
./bin/mongod -f shard/shard1-37011.conf 
./bin/mongod -f shard/shard1-37013.conf 
./bin/mongod -f shard/shard1-37015.conf
./bin/mongod -f shard/shard1-37017.conf
```
然后进入其中一个进行集群配置
```
 var cfg = {"_id":"shard1", "protocolVersion" : 1, "members":[ 
 {"_id":1,"host":"192.168.3.39:37011"}, 
 {"_id":2,"host":"192.168.3.39:37013"}, 
 {"_id":3,"host":"192.168.3.39:37015"}, 
 {"_id":4,"host":"192.168.3.39:37017", "arbiterOnly":true} 
 ]};
 rs.initiate(cfg) 
 rs.status()
```

### shard2-47011节点
```
dbpath=shard/shard2/shard2-47011
bind_ip=0.0.0.0 
port=47011
fork=true 
logpath=shard/shard2/shard2-47011.log 
replSet=shard2
shardsvr=true 
```
### shard2-47013节点
```
dbpath=shard/shard2/shard2-47013
bind_ip=0.0.0.0 
port=47013
fork=true 
logpath=shard/shard2/shard2-47013.log 
replSet=shard2
shardsvr=true 
```
### shard2-47015节点
```
dbpath=shard/shard2/shard2-47015
bind_ip=0.0.0.0 
port=47015
fork=true 
logpath=shard/shard2/shard2-47015.log 
replSet=shard2
shardsvr=true 
```
### shard2-47017节点
```
dbpath=shard/shard2/shard2-47017
bind_ip=0.0.0.0 
port=47017
fork=true 
logpath=shard/shard2/shard2-47017.log 
replSet=shard2
shardsvr=true
```
### 启动
```
./bin/mongod -f shard/shard2-47011.conf 
./bin/mongod -f shard/shard2-47013.conf 
./bin/mongod -f shard/shard2-47015.conf
./bin/mongod -f shard/shard2-47017.conf
```
然后进入其中一个进行集群配置
```
 var cfg = {"_id":"shard2", "protocolVersion" : 1, "members":[ 
 {"_id":1,"host":"192.168.3.39:47011"}, 
 {"_id":2,"host":"192.168.3.39:47013"}, 
 {"_id":3,"host":"192.168.3.39:47015"}, 
 {"_id":4,"host":"192.168.3.39:47017", "arbiterOnly":true} 
 ]};
 rs.initiate(cfg); 
 rs.status();
```

### shard3-57011节点
```
dbpath=shard/shard3/shard3-57011
bind_ip=0.0.0.0 
port=57011
fork=true 
logpath=shard/shard3/shard3-57011.log 
replSet=shard3
shardsvr=true 
```
### shard3-57013节点
```
dbpath=shard/shard3/shard3-57013
bind_ip=0.0.0.0 
port=57013
fork=true 
logpath=shard/shard3/shard3-57013.log 
replSet=shard3
shardsvr=true 
```
### shard3-57015节点
```
dbpath=shard/shard3/shard3-57015
bind_ip=0.0.0.0 
port=57015
fork=true 
logpath=shard/shard3/shard3-57015.log 
replSet=shard3
shardsvr=true 
```
### shard3-57017节点
```
dbpath=shard/shard3/shard3-57017
bind_ip=0.0.0.0 
port=57017
fork=true 
logpath=shard/shard3/shard3-57017.log 
replSet=shard3
shardsvr=true
```
### 启动
```
./bin/mongod -f shard/shard3-57011.conf 
./bin/mongod -f shard/shard3-57013.conf 
./bin/mongod -f shard/shard3-57015.conf
./bin/mongod -f shard/shard3-57017.conf
```
然后进入其中一个进行集群配置
```
 var cfg = {"_id":"shard3", "protocolVersion" : 1, "members":[ 
 {"_id":1,"host":"192.168.3.39:57011"}, 
 {"_id":2,"host":"192.168.3.39:57013"}, 
 {"_id":3,"host":"192.168.3.39:57015"}, 
 {"_id":4,"host":"192.168.3.39:57017", "arbiterOnly":true} 
 ]};
 rs.initiate(cfg);
 rs.status();
```


### shard4-58011节点
```
dbpath=shard/shard4/shard4-58011
bind_ip=0.0.0.0 
port=58011
fork=true 
logpath=shard/shard4/shard4-58011.log 
replSet=shard4
shardsvr=true 
```
### shard4-58013节点
```
dbpath=shard/shard4/shard4-58013
bind_ip=0.0.0.0 
port=58013
fork=true 
logpath=shard/shard4/shard4-58013.log 
replSet=shard4
shardsvr=true 
```
### shard4-58015节点
```
dbpath=shard/shard4/shard4-58015
bind_ip=0.0.0.0 
port=58015
fork=true 
logpath=shard/shard4/shard4-58015.log 
replSet=shard4
shardsvr=true 
```
### shard4-58017节点
```
dbpath=shard/shard4/shard4-58017
bind_ip=0.0.0.0 
port=58017
fork=true 
logpath=shard/shard4/shard4-58017.log 
replSet=shard4
shardsvr=true
```
### 启动
```
./bin/mongod -f shard/shard4-58011.conf 
./bin/mongod -f shard/shard4-58013.conf 
./bin/mongod -f shard/shard4-58015.conf
./bin/mongod -f shard/shard4-58017.conf
```
然后进入其中一个进行集群配置
```
 var cfg = {"_id":"shard4", "protocolVersion" : 1, "members":[ 
 {"_id":1,"host":"192.168.3.39:58011"}, 
 {"_id":2,"host":"192.168.3.39:58013"}, 
 {"_id":3,"host":"192.168.3.39:58015"}, 
 {"_id":4,"host":"192.168.3.39:58017", "arbiterOnly":true} 
 ]};
 rs.initiate(cfg);
 rs.status();
```

## 搭建路由节点
### 配置路由节点 route-27017.conf
```
port=27017 
bind_ip=0.0.0.0 
fork=true 
logpath=route/logs/route.log 
configdb=configsvr/192.168.3.39:17011,192.168.3.39:17013,192.168.3.39:17015
```
### 启动
```
./bin/mongos -f route/route-27017.conf
```
### 添加分片节点
```
./bin/mongo -port 27017

sh.status();

sh.addShard("shard1/192.168.3.39:37011,192.168.3.39:37013,192.168.3.39:37015,192.168.3.39:37017");
sh.addShard("shard2/192.168.3.39:47011,192.168.3.39:47013,192.168.3.39:47015,192.168.3.39:47017");
sh.addShard("shard3/192.168.3.39:57011,192.168.3.39:57013,192.168.3.39:57015,192.168.3.39:57017");
sh.addShard("shard4/192.168.3.39:58011,192.168.3.39:58013,192.168.3.39:58015,192.168.3.39:58017");
```

## 开启认证
### 创建管理员用户和普通用户
进入路由节点
```
./bin/mongo -p 27017

use admin;
db.createUser({user:"root",pwd:"123456",roles:[{role:"root",db:"admin"}]});

use lagou_resume;
db.createUser({
 user:"lagou_gx",
 pwd:"abc321",
 roles:[{role:"readWrite",db:"lagou_resume"}]
 });

```
### 关闭所有节点
```
killall mongod
```
### 生成密钥文件
```
openssl rand -base 64 756 > key.file
chmod 600 key.file
```
### 开启认证
配置文件加上认证选项， 路由节点不要加auth=true
```
auth=true
keyFile=key.file
```
重新启动集群
```
./bin/mongod -f config/config-17011.conf 
./bin/mongod -f config/config-17013.conf 
./bin/mongod -f config/config-17015.conf
./bin/mongod -f shard/shard1-37011.conf 
./bin/mongod -f shard/shard1-37013.conf 
./bin/mongod -f shard/shard1-37015.conf
./bin/mongod -f shard/shard1-37017.conf
./bin/mongod -f shard/shard2-47011.conf 
./bin/mongod -f shard/shard2-47013.conf 
./bin/mongod -f shard/shard2-47015.conf
./bin/mongod -f shard/shard2-47017.conf
./bin/mongod -f shard/shard3-57011.conf 
./bin/mongod -f shard/shard3-57013.conf 
./bin/mongod -f shard/shard3-57015.conf
./bin/mongod -f shard/shard3-57017.conf
./bin/mongod -f shard/shard4-58011.conf 
./bin/mongod -f shard/shard4-58013.conf 
./bin/mongod -f shard/shard4-58015.conf
./bin/mongod -f shard/shard4-58017.conf
```

# 2. 运行测试代码
### 运行单元测试代码
```
String name = "xdxtest";
        Resume resume  = new Resume();
        resume.setName(name);
        resume.setExpectSalary(99999);
        resume.setCity("sz");

        resumeRepository.save(resume);
        log.info("添加简历快照完成: {}", resume);
        List<Resume> resumeList = resumeRepository.findByNameEquals(name);
        log.info("姓名为{}的简历快照: {}", name, resumeList);
```
### 关键日志
```
2020-07-26 20:29:48.798  INFO 90799 --- [           main] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:2}] to 192.168.3.39:27017
2020-07-26 20:30:27.833  INFO 90799 --- [           main] com.lagou.ResumeRepositoryTest           : 添加简历快照完成: Resume{id='5f1d772ec20f3c62afbe607e', name='xdxtest', city='sz', birthday=null, expectSalary=99999.0}
2020-07-26 20:30:31.856  INFO 90799 --- [           main] com.lagou.ResumeRepositoryTest           : 姓名为xdxtest的简历快照: [Resume{id='5f1d772ec20f3c62afbe607e', name='xdxtest', city='sz', birthday=null, expectSalary=99999.0}]

```
### 通过客户端查看结果
```
mongodb://lagou_gx:abc321@192.168.3.39:27017/lagou_resume
```



















