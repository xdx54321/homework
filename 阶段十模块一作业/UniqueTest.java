package lagou;

import java.util.Arrays;

/**
 * 判断数组中所有的数字是否只出现一次。给定一个数组array，判断数组 array 中是否所有的数字都只 出现过一次。
 * 例如，arr = {1, 2, 3}，输出 YES。又如，arr = {1, 2, 1}，输出 NO。约束时间复杂度为 O(n)。
 */
public class UniqueTest {

    public static void main(String[] args) {
        {
            int[] arr = new int[] {1, 3, 4, 6, 3, 7, 2, 3, 4, 5};
            boolean isUnique = isUnique(arr);
            System.out.println("Array: " + Arrays.toString(arr));
            System.out.println(isUnique ? "YES" : "NO");
        }
        {
            int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9};
            boolean isUnique = isUnique(arr);
            System.out.println("Array: " + Arrays.toString(arr));
            System.out.println(isUnique ? "YES" : "NO");
        }
    }

    private static boolean isUnique(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            // 利用堆排序的特性
            if (!heapInsert(arr, i)) {
                return false;
            }
        }
        return true;
    }

    private static boolean heapInsert(int[] arr, int i) {
        while (i != 0) {
            int parent = (i - 1) / 2;
            if (arr[parent] < arr[i]) {
                int tmp = arr[parent];
                arr[parent] = arr[i];
                arr[i] = tmp;
                i = parent;
            } else if (arr[parent] == arr[i]) {
                return false;
            } else {
                break;
            }
        }
        return true;
    }

}
