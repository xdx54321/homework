package lagou;

import java.util.Arrays;

/**
 * 很久很久以前，有一位国王拥有5座金矿，每座金矿的黄金储量不同，需要参与挖掘的工人人数也不 同。
 * 例如有的金矿储量是500kg黄金，需要5个工人来挖掘；有的金矿储量是200kg黄金，需要3个工人 来挖掘…… 如果参与挖矿的工人的总数是10。
 * 每座金矿要么全挖，要么不挖，不能派出一半人挖取一半的金矿。要 求用程序求出，要想得到尽可能多的黄金，应该选择挖取哪几座金矿？
 */
public class GoldTest {

    /**
     *
     *
     * @param: w   工人数量
     * @param: n   可选金矿数量
     * @param: p[] 金矿开采所需的工人数量
     * @param: g[] 金矿储量
     */
    public static void main(String[] args) {
        {
            int workerCnt = 10;
            int[] workerNeeds = {5, 5, 3, 4, 3};
            int[] goldReserves = {400, 500, 200, 300, 350};
            System.out.println("工人总数: " + workerCnt);
            System.out.println("各金矿需求人数: " + Arrays.toString(workerNeeds));
            System.out.println("各金矿的收益为: " + Arrays.toString(goldReserves));
            System.out.println("最优受益: " + getBestGoldMining(workerCnt, workerNeeds, goldReserves));
        }
        System.out.println("-------------------");
        {
            int workerCnt = 8;
            int[] workerNeeds = {5, 5, 3, 4, 3};
            int[] goldReserves = {400, 500, 200, 300, 350};
            System.out.println("工人总数: " + workerCnt);
            System.out.println("各金矿需求人数: " + Arrays.toString(workerNeeds));
            System.out.println("各金矿的收益为: " + Arrays.toString(goldReserves));
            System.out.println("最优受益: " + getBestGoldMining(workerCnt, workerNeeds, goldReserves));
        }
    }

    /**
     * 获得金矿最优受益
     * @param workerCnt 工人数量
     * @param workerNeeds 金矿开采所需的工人数量
     * @param goldReserves 金矿储量
     * @return
     */
    private static int getBestGoldMining(int workerCnt, int[] workerNeeds, int[] goldReserves) {
        int[] results = new int[workerCnt + 1];
        for (int i = 1; i < goldReserves.length + 1; i++) {
            for (int j = workerCnt; j > 0; j--) {
                if (j >= workerNeeds[i - 1]) {
                    results[j] = Math.max(results[j],
                            results[j - workerNeeds[i - 1]] + goldReserves[i - 1]);
                }
            }
        }
        return results[workerCnt];
    }

}
