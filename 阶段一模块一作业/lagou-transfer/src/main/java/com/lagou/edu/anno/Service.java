package com.lagou.edu.anno;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author xudanxia
 * @Desc
 * @date 2020/3/4.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {

    String value() default "";

}
