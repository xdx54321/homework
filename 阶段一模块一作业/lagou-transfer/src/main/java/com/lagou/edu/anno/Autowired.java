package com.lagou.edu.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author xudanxia
 * @Desc
 * @date 2020/3/4.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Autowired {
}
