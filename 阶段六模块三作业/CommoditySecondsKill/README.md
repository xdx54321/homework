# CommoditySecondsKill

#### 项目介绍
springMVC+MyBatis+Redis+bootstrap实现的一个简单的商品秒杀dome

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. 数据库脚本\src\main\sql
2. spring相关配置\src\main\resources\spring
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 技术回顾

1. 前端交互设计过程
2. Restful接口设计(https://www.runoob.com/w3cnote/restful-architecture.html)
3. SpringMVC使用技巧
    1): SpringMVC配置和运行流程（http://img.my.csdn.net/uploads/201304/13/1365825529_4693.png）
    2): DTO传递数据
    3): 注解映射驱动
4. Bootstrap和js使用(https://v3.bootcss.com/)

前端交互设计：
                           
                            1：获取标准系统时间
                                 ⬇
                 （未开始）       ⬇                       （结束）
         倒计时  ⬅⬅⬅⬅⬅⬅ 2：时间判断(开始时间;结束时间) ➡➡➡➡➡ 秒杀结束      
           ⬇                     ⬇
           ⬇                     ⬇(已开始)
           ⬇                     ⬇  
           ⬇                3:秒杀地址 ➡➡ 4：执行秒杀 ➡➡ 结果      
           ⬇  
           ⬇  
         3:秒杀地址 ➡➡ 4：执行秒杀 ➡➡ 结果  
     
     
     


     
     
     
        