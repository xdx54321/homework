package org.seckill.dao.cache;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.seckill.dao.SeckillDao;
import org.seckill.entity.Seckill;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @author Adger
 * @Title: RedisDaoTest
 * @ProjectName myweb
 * @Description: TODO
 * @date 2018/9/11 13:07
 */


@RunWith(SpringJUnit4ClassRunner.class)
/*告诉junit spring的配置文件*/
@ContextConfiguration("classpath:spring/spring-dao.xml")
public class RedisDaoTest {

    private long id = 1001;

    /*注入dao实现类依赖*/
    @Resource
    private RedisDao redisDao;

    /*注入dao实现类依赖*/
    @Resource
    private SeckillDao seckillDao;


    @Test
    public void testSeckill() throws Exception {
        //get and put
        Seckill seckill = redisDao.getSeckill(id);
        if (seckill == null) {
            seckill = seckillDao.queryById(id);
            if (seckill != null) {
                String result = redisDao.putSeckill(seckill);
                System.out.println("result =>" + result);
                seckill = redisDao.getSeckill(id);
                System.out.println("seckill" + seckill);
            }
        }

    }

}