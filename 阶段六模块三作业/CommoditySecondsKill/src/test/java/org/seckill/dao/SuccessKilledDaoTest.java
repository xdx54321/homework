package org.seckill.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.seckill.entity.SuccessKilled;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @author Adger
 * @Title: SuccessKilledDaoTest
 * @ProjectName myweb
 * @Description: TODO
 * @date 2018/8/31 14:27
 */


/**
 *  配置spring和junit整合  junit启动时加载springIOC容器
 *  spring-test; junit4
 */
@RunWith(SpringJUnit4ClassRunner.class)
/*告诉junit spring的配置文件*/
@ContextConfiguration("classpath:spring/spring-dao.xml")
public class SuccessKilledDaoTest {

    @Resource
    private SuccessKilledDao successKilledDao;

    @Test
    public void queryByIdWithSeckill() {
        long id = 1001;
        String phone = "15828911094L";
        SuccessKilled successKilled = successKilledDao.queryByIdWithSeckill(id,phone);
        System.out.println(successKilled);
        System.out.println(successKilled.getSeckill());
    }


    @Test
    public void insertSuccessKilled() {
        /*第一次insertCount=1
        * 第二次insertCount=0
        */
        long id = 1001;
        String phone = "15828911094L";
        int insertCount = successKilledDao.insertSuccessKilled(id, phone);
        System.out.println("insertCount==>" + insertCount);
    }


}