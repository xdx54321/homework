package org.seckill.service;

import org.seckill.dto.Exposer;
import org.seckill.dto.SeckillExecution;
import org.seckill.entity.Seckill;
import org.seckill.entity.SuccessKilled;
import org.seckill.exception.RepeatKillException;
import org.seckill.exception.SeckillCloseException;

import java.util.List;

public interface SeckillService {

    /**
     * 查询所有秒杀接口
     * @return
     */
    List<Seckill> getSeckillList();


    /**
     * 查询单个秒杀记录
     * @param seckillId
     * @return
     */
    Seckill getById(long seckillId);

    /**
     * 秒杀开启时输出秒杀接口地址，
     * 否则输出系统时间和秒杀时间
     * @param seckillId
     */
    Exposer exportSeckillUrl(long seckillId);

    /**
     * 执行秒杀操作
     * @param seckillId
     * @param userPhone
     * @param md5
     */
    SeckillExecution executeSeckill(long seckillId, String userPhone, String md5);

    /**
     * 执行秒杀操作(内部)
     * @param seckillId
     * @param userPhone
     */
    void executeSeckill(long seckillId, String userPhone);

    /**
     * 获取秒杀结果
     * @param seckillId
     * @param userPhone
     * @return
     */
    SuccessKilled getSecKillResult(long seckillId, String userPhone);

}
