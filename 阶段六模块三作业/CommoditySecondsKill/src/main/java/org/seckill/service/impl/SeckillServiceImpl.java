package org.seckill.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.client.producer.SendResult;
import com.alibaba.rocketmq.client.producer.SendStatus;
import com.alibaba.rocketmq.common.message.Message;
import org.apache.commons.collections.MapUtils;
import org.seckill.dao.SeckillDao;
import org.seckill.dao.SuccessKilledDao;
import org.seckill.dao.cache.RedisDao;
import org.seckill.dto.Exposer;
import org.seckill.dto.SeckillExecution;
import org.seckill.entity.Seckill;
import org.seckill.entity.SuccessKilled;
import org.seckill.enums.SeckillStatEnum;
import org.seckill.exception.RepeatKillException;
import org.seckill.exception.SeckillCloseException;
import org.seckill.exception.SeckillException;
import org.seckill.mq.SecKillProducer;
import org.seckill.service.SeckillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SeckillServiceImpl implements SeckillService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SeckillDao seckillDao;

    @Autowired
    private SuccessKilledDao successKilledDao;

    @Autowired
    private RedisDao redisDao;

    @Resource
    private SecKillProducer secKillProducer;

    // md5盐值字符串，用于混淆MD5
    private final String slat = "QYEUWIEQWEQWJJEWJEQE134564@$$#@@#@$68464FDFS";

    @PostConstruct
    private void init() {
        // 初始化库存
        seckillDao.queryAll(0, 4).forEach(seckill -> {
            redisDao.putSeckill(seckill);
            redisDao.setStock(seckill.getSeckillId(), seckill.getNumber());
        });
    }

    @Override
    public List<Seckill> getSeckillList() {
        return seckillDao.queryAll(0, 4);
    }

    @Override
    public Seckill getById(long seckillId) {
        return seckillDao.queryById(seckillId);
    }

    @Override
    public Exposer exportSeckillUrl(long seckillId) {

        Seckill seckill = redisDao.getSeckill(seckillId);
        if (seckill == null) {
           throw new SeckillException("seckillId not exist");
        }
        Date startTime = seckill.getStartTime();
        Date endTime = seckill.getEndTime();
        //系统当前时间
        Date nowTime = new Date();
        if (nowTime.getTime() < startTime.getTime() || nowTime.getTime() > endTime.getTime()) {
            return new Exposer(false, seckillId, nowTime.getTime(), startTime.getTime(), endTime.getTime());
        }

        String md5 = getMd5(seckillId);
        return new Exposer(true, md5, seckillId);
    }

    @Override
    public SeckillExecution executeSeckill(long seckillId, String userPhone, String md5) {

        if (md5 == null || !md5.equals(getMd5(seckillId))) {
            throw new SeckillException("seckill data rewrite");
        }
        // 先预扣库存, 库存不足则抛异常
        long stock = redisDao.decr(seckillId);
        if (stock < 0) {
            throw new SeckillCloseException("seckill is close");
        }

        // 再发消息队列
        SuccessKilled seckill = new SuccessKilled();
        seckill.setSeckillId(seckillId);
        seckill.setUserPhone(userPhone);
        Message msg = new Message("SecKill", "MyTag", JSON.toJSONString(seckill).getBytes());
        SendResult sendResult = null;
        try {
            sendResult = secKillProducer.getDefaultMQProducer().send(msg);
        } catch (Exception e) {
            logger.error(e.getMessage() + sendResult);
            throw new SeckillException("seckill exception");
        }
        if (sendResult == null || sendResult.getSendStatus() != SendStatus.SEND_OK) {
            throw new SeckillException("seckill exception");
        }

        return new SeckillExecution(seckillId, SeckillStatEnum.SECKILLING);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void executeSeckill(long seckillId, String userPhone) {

        // 执行秒杀逻辑：减库存 + 记录购买行为
        Date nowTime = new Date();
        // 记录购买行为
        int insertCount = successKilledDao.insertSuccessKilled(seckillId, userPhone);
        // 唯一：seckillId,userPhone
        if (insertCount <= 0) {
            // 重复秒杀, 记录状态
            logger.info("重复秒杀, id: {}, phone: {}", seckillId, userPhone);
            throw new RepeatKillException("repeat commit");
        }

        // 减库存,热点商品竞争
        int updataCount = seckillDao.reduceNumber(seckillId, nowTime);
        if (updataCount <= 0) {
            // 没有更新到记录，秒杀结束  rollback
            logger.info("秒杀已结束, id: {}, phone: {}", seckillId, userPhone);
            successKilledDao.updateSecKillState(seckillId, userPhone, -1);
            return;
        }

        // 秒杀成功, 记录状态
        successKilledDao.updateSecKillState(seckillId, userPhone, 1);
        logger.info("秒杀成功, id: {}, phone: {}", seckillId, userPhone);

    }

    /**
     * 获取秒杀结果
     * @param seckillId
     * @param userPhone
     * @return
     */
    @Override
    public SuccessKilled getSecKillResult(long seckillId, String userPhone) {
        return successKilledDao.queryByIdWithSeckill(seckillId, userPhone);
    }

    private String getMd5(long seckillId) {
        String base = seckillId + "/" + slat;
        String md5 = DigestUtils.md5DigestAsHex(base.getBytes());
        return md5;
    }

}
