package org.seckill.mq;

import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.client.producer.DefaultMQProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SecKillProducer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private DefaultMQProducer defaultMQProducer;
    private String producerGroup = "seckill";
    private String namesrvAddr = "127.0.0.1:9876";;

    /**
     * Spring bean init-method
     */
    @PostConstruct
    public void init() throws MQClientException {
        // 参数信息
        logger.info("DefaultMQProducer initialize!");
        logger.info(producerGroup);
        logger.info(namesrvAddr);

        // 初始化
        defaultMQProducer = new DefaultMQProducer(producerGroup);
        defaultMQProducer.setNamesrvAddr(namesrvAddr);
        defaultMQProducer.setInstanceName(String.valueOf(System.currentTimeMillis()));

        defaultMQProducer.start();

        logger.info("DefaultMQProudcer start success!");

    }

    public DefaultMQProducer getDefaultMQProducer() {
        return defaultMQProducer;
    }

}
