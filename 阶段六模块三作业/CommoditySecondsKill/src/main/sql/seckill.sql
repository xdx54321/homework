-- 数据库初始化脚本

-- 创建数据库
CREATE DATABASE seckill;

-- 使用数据库
use seckill;

-- 创建秒杀库存表
CREATE TABLE `seckill` (
                           `seckill_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品库存id',
                           `name` varchar(120) DEFAULT NULL COMMENT '商品名称',
                           `number` int(11) DEFAULT NULL COMMENT '库存数量',
                           `start_time` datetime DEFAULT NULL COMMENT '秒杀开启时间',
                           `end_time` datetime DEFAULT NULL COMMENT '秒杀结束时间',
                           `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                           PRIMARY KEY (`seckill_id`),
                           KEY `idx_start_time` (`start_time`) USING BTREE,
                           KEY `idx_end_time` (`end_time`) USING BTREE,
                           KEY `idx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=utf8 COMMENT='秒杀库存表';


-- 初始化数据
INSERT INTO `seckill` VALUES ('1000', '1000元秒杀iphone6', '79', '2020-10-06 13:00:00', '2020-10-06 14:00:00', '2020-10-06 13:00:00');
INSERT INTO `seckill` VALUES ('1001', '500元秒杀ipad2', '200', '2020-10-06 13:00:00', '2020-10-06 14:00:00', '2020-10-06 13:00:00');
INSERT INTO `seckill` VALUES ('1002', '300元秒杀小米4', '300', '2020-10-06 13:00:00', '2020-10-06 14:00:00', '2020-10-06 13:00:00');
INSERT INTO `seckill` VALUES ('1003', '200元秒杀红米note', '400', '2020-10-06 13:00:00', '2020-10-06 14:00:00', '2020-10-06 13:00:00');

-- 秒杀成功明细表
-- 用户登录认证相关信息
CREATE TABLE `success_killed` (
                                  `seckill_id` bigint(20) NOT NULL COMMENT '秒杀商品id',
                                  `user_phone` bigint(20) NOT NULL COMMENT '用户手机号',
                                  `state` tinyint(4) DEFAULT '-1' COMMENT '状态标识： -1：无效  0：成功  1：已付款  2：已发货',
                                  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                                  PRIMARY KEY (`seckill_id`,`user_phone`),
                                  KEY `idx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='秒杀成功明细表';
