# 基于消息队列+缓存的秒杀案例

视频演示: 链接: https://pan.baidu.com/s/1kF5jXWQtQTPsG04b-dhB-w  密码: a3no

# 环境搭建
Mac  JDK1.8 
## RocketMQ
下载rocketMQ 并解压
https://mirrors.bfsu.edu.cn/apache/rocketmq/4.7.1/rocketmq-all-4.7.1-bin-release.zip
启动Namesrv
```
nohup sh bin/mqbroker -n localhost:9876 &
```
启动Broker
```
nohup sh bin/mqbroker -n localhost:9876 &
```

# 流程说明
1. 服务启动, 加载秒杀商品至Redis
2. 请求秒杀地址
3. 开始秒杀, 从redis检查库存数据, 并把请求发到MQ
4. MQ消费秒杀消息, 执行秒杀逻辑
5. 前端请求接口查询秒杀结果


# SQL
```
-- 创建数据库
CREATE DATABASE seckill;

-- 使用数据库
use seckill;

-- 创建秒杀库存表
CREATE TABLE `seckill` (
                           `seckill_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品库存id',
                           `name` varchar(120) DEFAULT NULL COMMENT '商品名称',
                           `number` int(11) DEFAULT NULL COMMENT '库存数量',
                           `start_time` datetime DEFAULT NULL COMMENT '秒杀开启时间',
                           `end_time` datetime DEFAULT NULL COMMENT '秒杀结束时间',
                           `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                           PRIMARY KEY (`seckill_id`),
                           KEY `idx_start_time` (`start_time`) USING BTREE,
                           KEY `idx_end_time` (`end_time`) USING BTREE,
                           KEY `idx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=utf8 COMMENT='秒杀库存表';


-- 初始化数据
INSERT INTO `seckill` VALUES ('1000', '1000元秒杀iphone6', '79', '2020-10-06 13:00:00', '2020-10-06 14:00:00', '2020-10-06 13:00:00');

-- 秒杀成功明细表
CREATE TABLE `success_killed` (
                                  `seckill_id` bigint(20) NOT NULL COMMENT '秒杀商品id',
                                  `user_phone` VARCHAR(20) NOT NULL COMMENT '用户手机号',
                                  `state` tinyint(4) DEFAULT '-1' COMMENT '状态标识： -1：无效  0：成功  1：已付款  2：已发货',
                                  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                                  PRIMARY KEY (`seckill_id`,`user_phone`),
                                  KEY `idx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='秒杀成功明细表';

```
