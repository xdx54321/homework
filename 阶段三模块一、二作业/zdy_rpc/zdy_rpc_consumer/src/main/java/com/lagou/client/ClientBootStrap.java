package com.lagou.client;

import com.lagou.service.UserService;

public class ClientBootStrap {

    public static void main(String[] args) throws InterruptedException {

        RpcConsumer rpcConsumer = new RpcConsumer();
        rpcConsumer.initClient();
        UserService proxy = (UserService) rpcConsumer.createProxy(UserService.class);

        while (true) {
            System.out.println(proxy.sayHello("are you ok?"));
            Thread.sleep(6000);
        }

    }

}
