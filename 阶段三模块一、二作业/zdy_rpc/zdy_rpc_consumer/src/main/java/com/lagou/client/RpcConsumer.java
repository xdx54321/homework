package com.lagou.client;

import com.lagou.service.JSONSerializer;
import com.lagou.service.RpcEncoder;
import com.lagou.service.RpcRequest;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import org.I0Itec.zkclient.ZkClient;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

public class RpcConsumer {

    //创建线程池对象
    private static ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private final static String DURATION_PATH = "/rpc/request/duration";

    private final static String SERVER_PATH = "/rpc/server";

    private ZkClient zkClient = new ZkClient("127.0.0.1:2181", 5000, 3000);

    //1.创建一个代理对象
    public Object createProxy(final Class<?> serviceClass) {
        //借助JDK动态代理生成代理对象
        return Proxy.newProxyInstance(serviceClass.getClassLoader(), new Class<?>[]{serviceClass}, new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                // 设置参数
                RpcRequest request = new RpcRequest();
                request.setClassName(serviceClass.getName());
                request.setMethodName(method.getName());
                request.setParameters(args);
                request.setParameterTypes(method.getParameterTypes());
                request.setRequestId(UUID.randomUUID().toString());
                // 去服务端请求数据
                RpcConnection connection = getConnection();
                if (connection == null) {
                    throw new RuntimeException("找不到可用的服务器");
                }
                connection.setPara(request);
                System.out.println("发起请求: " + request + ", IP: " + connection.getIp() + ", 端口: " + connection.getPort());
                long begin = System.currentTimeMillis();
                Object result = executor.submit(connection).get();
                long duration = System.currentTimeMillis() - begin;
                connection.setPara(null);
                String nodeName = connection.getIp() + ":" + connection.getPort();
                try {
                    if (!zkClient.exists(DURATION_PATH + "/" + nodeName)) {
                        zkClient.createEphemeral(DURATION_PATH + "/" + nodeName, String.valueOf(duration).getBytes());
                    } else {
                        zkClient.writeData(DURATION_PATH + "/" + nodeName, String.valueOf(duration).getBytes());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return result;
            }
        });

    }

    //2.初始化netty客户端
    public void initClient() {
        // 从zk获取服务器列表, 并与之建立连接
        try {
            List<String> children = zkClient.getChildren(SERVER_PATH);
            for (String child : children) {
                String ip = child.split(":")[0];
                int port = Integer.parseInt(child.split(":")[1]);
                RpcConnection connection = connect(ip, port);
                connectPool.put(child, connection);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        zkClient.subscribeChildChanges(SERVER_PATH, (s, list) -> {
            for (String node : list) {
                String ip = node.split(":")[0];
                int port = Integer.parseInt(node.split(":")[1]);
                RpcConnection conn = connectPool.get(s);
                if (conn != null) {
                    conn.close();
                }
                conn = connect(ip, port);
                System.out.println("重新连接, ip: " + s + ", port: " + port);
                connectPool.put(node, conn);
            }
        });

    }

    private RpcConnection connect(String ip, int port) throws InterruptedException {
        RpcConnection connection = new RpcConnection();
        connection.setIp(ip);
        connection.setPort(port);
        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 2000)
                .option(ChannelOption.SO_TIMEOUT, 3000)
                .handler(new ChannelInitializer<SocketChannel>() {
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new RpcEncoder(RpcRequest.class, new JSONSerializer()));
                        pipeline.addLast(new StringDecoder());
                        pipeline.addLast(connection);
                    }
                });
        bootstrap.connect(ip, port).sync();
        connection.setGroup(group);
        return connection;
    }

    private static Map<String, RpcConnection> connectPool = new HashMap<>();

    private static ThreadLocalRandom random = ThreadLocalRandom.current();

    private RpcConnection getConnection() {
        // 负责从"连接池"获取连接
        if (connectPool.isEmpty()) {
            return null;
        }
        int minDurations = Integer.MAX_VALUE;
        String nodeName = null;
        List<String> children = zkClient.getChildren(DURATION_PATH);
        if (children != null && children.size() > 1) {
            for (String child : children) {
                try {
                    if (!zkClient.exists(DURATION_PATH + "/" + child)) {
                        continue;
                    }
                    byte[] data = zkClient.readData(DURATION_PATH + "/" + child);
                    int duration = Integer.parseInt(new String(data));
                    if (duration < minDurations) {
                        minDurations = duration;
                        nodeName = child;
                    }
                } catch (Exception e) {}
            }
        }

        if (nodeName == null || !connectPool.containsKey(nodeName)) {
            int index = random.nextInt(0, connectPool.keySet().size());
            nodeName = (String) connectPool.keySet().toArray()[index];
        }
        synchronized (nodeName) {
            return connectPool.get(nodeName);
        }
    }

}
