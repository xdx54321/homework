package com.lagou.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.EventLoopGroup;
import lombok.Data;

import java.util.concurrent.Callable;

@Data
public class RpcConnection extends ChannelInboundHandlerAdapter implements Callable  {

    private String ip;

    private int port;

    private EventLoopGroup group;

    public void close() {
        group.shutdownGracefully();
    }

    private ChannelHandlerContext context;
    private String result;
    private Object para;

    public void channelActive(ChannelHandlerContext ctx) {
        context = ctx;
    }

    /**
     * 收到服务端数据，唤醒等待线程
     */
    public synchronized void channelRead(ChannelHandlerContext ctx, Object msg) {
        result = msg.toString();
        notify();
    }

    /**
     * 写出数据，开始等待唤醒
     */
    public synchronized Object call() throws InterruptedException {
        context.writeAndFlush(para);
        wait(3000L);
        return result;
    }

    /*
     设置参数
     */
    void setPara(Object para) {
        this.para = para;
    }

}
