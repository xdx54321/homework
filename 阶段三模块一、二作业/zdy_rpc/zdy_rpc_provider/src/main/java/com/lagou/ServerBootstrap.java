package com.lagou;

import com.lagou.handler.UserServerHandler;
import com.lagou.util.IpUtils;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.ThreadLocalRandom;

@SpringBootApplication
public class ServerBootstrap {

    private static ZkClient zkClient = new ZkClient("10.224.36.226:2181");

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ServerBootstrap.class, args);
        ThreadLocalRandom random = ThreadLocalRandom.current();
        String ip = IpUtils.getLocalIpByNetcard();
        System.out.println("本机IP: " + ip);
        int port = random.nextInt(8990, 32000);
        UserServerHandler.startServer(ip, port);
        String path = "/rpc/server";
        if (!zkClient.exists(path)) {
            zkClient.createPersistent(path, true);
        }
        if (zkClient.exists(path + "/" + ip)) {
            zkClient.delete(path + "/" + ip);
        }
        String nodeName = ip + ":" + port;
        zkClient.createEphemeral(path + "/" + nodeName, String.valueOf(port).getBytes());
        Runtime.getRuntime().addShutdownHook(new Thread(() -> zkClient.delete(path + "/" + ip)));
    }

}
