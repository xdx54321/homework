package com.lagou.handler;

import com.lagou.service.*;
import com.lagou.util.ApplicationContextHolder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
public class UserServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if (!(msg instanceof RpcRequest)) {
            return;
        }

        RpcRequest request = (RpcRequest) msg;
        Class clazz = Class.forName(request.getClassName());
        UserService target = (UserService) ApplicationContextHolder.getContext().getBean(clazz);
        Method method = target.getClass().getMethod(request.getMethodName(), request.getParameterTypes());
        Object result = method.invoke(target, request.getParameters());
        ctx.writeAndFlush(result);

    }

    // hostName:ip地址  port:端口号
    public static void startServer(String hostName, int port) throws InterruptedException {

        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();

        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new RpcEncoder(RpcRequest.class, new JSONSerializer()));
                        pipeline.addLast(new RpcDecoder(RpcRequest.class, new JSONSerializer()));
                        pipeline.addLast(new UserServerHandler());
                    }
                });
        serverBootstrap.bind(hostName, port).sync();
        // TODO: 将自己的IP端口注册到zk(临时节点)
    }

}
