# Docker作业

演示视频
链接: https://pan.baidu.com/s/1YztsrOzKqhd0ZR51GY0Exw  密码: dauq

# 环境搭建

基础环境: Mac

## 安装Docker
安装docker
```
brew cask install docker
```
配置镜像加速
```
# 在任务栏点击 Docker for mac 应用图标 -> Perferences... -> Docker Engine 在列表中填写加速器地址即可
{
  "debug": true,
  "experimental": false,
  "registry-mirrors": [
    "https://docker.mirrors.ustc.edu.cn",
    "https://hub-mirror.c.163.com"
  ]
}
```

## 安装docker-machine
```
sudo curl -L https://github.com/docker/machine/releases/download/v0.16.1/docker-machine-`uname -s`-`uname -m` > /usr/local/bin/docker-machine
sudo chmod +x /usr/local/bin/docker-machine
docker-machine -v
```

# 部署服务

准备可运行的jar包 hot.jar

## 制作镜像

### 创建Dockerfile
```
FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD hot.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```

### 生成镜像
```
docker build -t hot .
```

## 创建Swarm集群
```
docker-machine create -d virtualbox manager
```

## 部署服务
```
docker service create --replicas 1 -p 6379:6379 --name redis redis:3
docker service create --replicas 1 -p 3306:3306 -e MYSQL_ROOT_PASSWORD=xxx123456 --name mysql mysql:5.7
docker service create --replicas 1 -p 8088:8088 --name hot hot
```

