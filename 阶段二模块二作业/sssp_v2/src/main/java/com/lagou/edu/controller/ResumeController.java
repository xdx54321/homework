package com.lagou.edu.controller;

import com.lagou.edu.pojo.Resume;
import com.lagou.edu.service.ResumeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/resume")
public class ResumeController {

    @Resource
    private ResumeService resumeService;

    @RequestMapping("/list")
    public ModelAndView list(HttpServletRequest request) {

        List<Resume> resumeList = resumeService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("resumes", resumeList);
        request.setAttribute("resumes", resumeList);
        modelAndView.setViewName("list");
        return modelAndView;
    }

    @RequestMapping("/delete")
    public ModelAndView delete(Long id, HttpServletRequest request) {

        resumeService.delete(id);
        request.setAttribute("resumes", resumeService.findAll());

        ModelAndView modelAndView = new ModelAndView();
        // 视图信息(封装跳转的页面信息) 逻辑视图名
        modelAndView.setViewName("list");
        return modelAndView;
    }

    @RequestMapping("/save")
    public String save(Resume resume) {
        resumeService.save(resume);
        return "redirect:list";
    }

    @RequestMapping("/add")
    public ModelAndView add() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("add");
        return modelAndView;
    }

    @RequestMapping("/update")
    public ModelAndView update(Resume resume, HttpServletRequest request) {

        request.setAttribute("id", resume.getId());
        request.setAttribute("name", resume.getName());
        request.setAttribute("address", resume.getAddress());
        request.setAttribute("phone", resume.getPhone());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("modify");
        return modelAndView;
    }

}
