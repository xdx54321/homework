http://localhost:80/sssp_v2/resume/list

tomcat配置保持默认

nginx配置
```
  upstream lagouServer {
        server 10.224.36.111:8080;
        server 10.224.37.192:8080;
    }

     server {
        listen       80;
        server_name  lagouServer;

        location / {
                proxy_pass http://lagouServer;
                proxy_redirect default;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
             root   html;
        }
    }
```