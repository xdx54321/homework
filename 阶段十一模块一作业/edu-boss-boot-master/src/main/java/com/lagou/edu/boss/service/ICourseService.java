package com.lagou.edu.boss.service;

import org.jsoup.nodes.Element;

public interface ICourseService {
    void removeImgWidthAttribute(Element element);

    /**
     * 课程上下架
     * @param courseId
     * @param status
     * @return
     */
    boolean changeState(Integer courseId, Integer status);

}
