package com.lagou.edu.message.mq.listener;

import com.lagou.edu.common.constant.MQConstant;
import com.lagou.edu.common.mq.dto.BaseMqDTO;
import com.lagou.edu.common.mq.listener.AbstractMqListener;
import com.lagou.edu.common.util.ValidateUtils;
import com.lagou.edu.message.api.dto.Message;
import com.lagou.edu.message.service.IMessageService;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;


@Slf4j
@Component
@RocketMQMessageListener(topic = MQConstant.Topic.COURSE_RELEASE_SEND_MESSAGE, consumerGroup = "${rocketmq.producer.group}" + "_" + MQConstant.Topic.COURSE_RELEASE_SEND_MESSAGE)
public class CourseReleaseSendMessageListener extends AbstractMqListener<BaseMqDTO<List<Integer>>> implements RocketMQListener<BaseMqDTO<List<Integer>>>{

    @Autowired
    private IMessageService messageService;

    @Override
    public void onMessage(BaseMqDTO<List<Integer>> data) {
        //校验参数
        ValidateUtils.notNullParam(data);
        ValidateUtils.notNullParam(data.getMessageId());

        if(this.checkMessageId(data.getMessageId())) {
            return;
        }

        List<Integer> userIdList = data.getData();
        if(Collections.isEmpty(userIdList)) {
            return;
        }

        //调用PushServer通过netty-socketio推送给前台用户
        Message message = null;
        for (Integer userId : userIdList) {
            message = new Message();
            message.setUserId(userId);
            messageService.sendMessage(message);
        }
        this.updateMessageId(data.getMessageId());
    }
}