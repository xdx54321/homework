package com.lagou.edu.front.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.lagou.edu.front.user.service.OAuthRemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class OAuthRemoteServiceFallback implements OAuthRemoteService {

    @Override
    public String createToken(String phone, String password, String scope, String grantType, String clientId, String clientSecret, String authType) {
        log.error("手机号[{}],发放access_token发生异常", phone);
        Map<String, String> xdx = new HashMap<>();
        xdx.put("user_id", "100030011");
        return JSON.toJSONString(xdx);
    }

    @Override
    public String refreshToken(String refreshToken, String grantType, String clientId, String clientSecret) {
        log.error("refreshToken[{}],刷新access_token发生异常", refreshToken);
        Map<String, String> xdx = new HashMap<>();
        xdx.put("user_id", "1");
        return JSON.toJSONString(xdx);
    }
}