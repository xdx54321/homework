# 作业内容

后台管理系统对课程信息的新增、编辑、列表展示、以及课程的上架和下架服务。

# 演示视频

课程管理
链接: https://pan.baidu.com/s/1vveq0cOxUUeuBeC2HaN3SA  密码: mdm0

消息通知
链接: https://pan.baidu.com/s/1JjdqqyfKdmW6tErYW3C4rg  密码: 0bra

# 课程管理功能

在项目现有的系统架构基础上，完成课程模块的遗留功能，主要包括后台管理系统对课程信息的新增、编辑、列表展示、以及课程的上架和下架服务。

## 查询课程
```
curl --location --request POST 'http://127.0.0.1:8010/course/getQueryCourses' \
--header 'Content-Type: application/json' \
--data-raw '{
    "currentPage": 0,
    "pageSize": 10,
    "courseName": "",
    "status": 1
}'
```

## 新增课程
```
curl --location --request POST 'http://127.0.0.1:8010/course/saveOrUpdateCourse' \
--header 'Content-Type: application/json' \
--data-raw '{
    "brief": "测试课程",
    "courseDescriptionMarkDown": "测试课程内容",
    "courseImgUrl": "https://edu-lagou.oss-cn-beijing.aliyuncs.con/images/2020/11/15/16054504044207880.jpg",
    "courseListImg": "https://edu-1agou.oss-cn-beijing.aliyuncs.com/inages/2020/11/15/16054584082605716.jpg",
    "courseName": "测试课程1002",
    "discounts": 12.2,
    "discountsTag": "秒杀",
    "id": "",
    "previewFirstField": "测试课程1218",
    "previ eSecondField": "测试课程1218",
    "price": "99",
    "priceTag": "xdx",
    "sales": "22",
    "sortNum": "199",
    "status": 0,
    "teacherDTO": {
        "teacherName": "xdx",
        "position": "java开发工程师",
        "description": "测试"
    },
    "activityCourse": false,
    "activityCourseDTo": {}
}'
```

## 编辑课程
```
curl --location --request POST 'http://127.0.0.1:8010/course/saveOrUpdateCourse' \
--header 'Content-Type: application/json' \
--data-raw '{
    "price": 88,
    "id": "18"
}'
```

## 上架课程
```
curl --location --request GET 'http://127.0.0.1:8010/course/changeState?courseId=18&status=1'
```

## 下架课程
```
curl --location --request GET 'http://127.0.0.1:8010/course/changeState?courseId=18&status=0'
```

# 消息推送功能

课程上架成功的同时，以异步方式向当前系统的所有登录用户推送消息，浏览器显示“xxx课程新上线，欢迎试学。”











