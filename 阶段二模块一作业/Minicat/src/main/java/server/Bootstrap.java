package server;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import server.mapper.Context;
import server.mapper.Host;
import server.mapper.Mapper;
import server.mapper.Wrapper;

import java.io.*;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Minicat的主类
 */
public class Bootstrap {

    private Mapper projectMapper;

    /**
     * 定义socket监听的端口号
     */
    private int port = 8080;

    /**
     * Minicat启动需要初始化展开的一些操作
     */
    public void start() throws Exception {

        projectMapper = initServer();

        // 定义一个线程池
        int corePoolSize = 10;
        int maximumPoolSize = 50;
        long keepAliveTime = 100L;
        TimeUnit unit = TimeUnit.SECONDS;
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(50);
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                unit,
                workQueue,
                threadFactory,
                handler
        );

        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("=====>>>Minicat start on port：" + port);

        /*
            多线程改造（使用线程池）
         */
        while (true) {
            Socket socket = serverSocket.accept();
            RequestProcessor requestProcessor = new RequestProcessor(socket, projectMapper);
            threadPoolExecutor.execute(requestProcessor);
        }

    }

    private Mapper initServer() {

        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("server.xml");
        SAXReader saxReader = new SAXReader();
        Mapper mapper = new Mapper();

        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();

            List<Element> selectNodes = rootElement.selectNodes("//service");
            for (int i = 0; i < selectNodes.size(); i++) {
                Element element = selectNodes.get(i);
                // <servlet-name>lagou</servlet-name>
                Element connectorElement = (Element) element.selectSingleNode("Connector");
                String port = connectorElement.attribute("port").getValue();

                // <servlet-class>server.LagouServlet</servlet-class>
                Element engineElement = (Element) element.selectSingleNode("Engine");
                Element hostElement = engineElement.element("Host");
                String hostname = hostElement.attribute("name").getValue();
                String appBase = hostElement.attribute("appBase").getValue();

                // 遍历appBase下的目录
                File path = new File(appBase);
                if (path.isDirectory()) {
                    for (File file : path.listFiles()) {
                        if (!file.isDirectory()) {
                            continue;
                        }
                        Host host = new Host();
                        host.setPort(Integer.parseInt(port));
                        host.setHostname(hostname);
                        host.setAppBase(appBase);
                        host.setContext(loadServlet(appBase + "/" + file.getName() + "/web.xml"));
                        mapper.add(file.getName(), host);
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mapper;
    }

    /**
     * 加载解析web.xml，初始化Servlet
     */
    private Context loadServlet(String webXmlPath) throws FileNotFoundException {

        InputStream resourceAsStream = new FileInputStream(webXmlPath);
        SAXReader saxReader = new SAXReader();

        Context context = new Context();

        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();

            List<Element> selectNodes = rootElement.selectNodes("//servlet");
            for (int i = 0; i < selectNodes.size(); i++) {
                Element element = selectNodes.get(i);
                // <servlet-name>lagou</servlet-name>
                Element servletnameElement = (Element) element.selectSingleNode("servlet-name");
                String servletName = servletnameElement.getStringValue();
                // <servlet-class>server.LagouServlet</servlet-class>
                Element servletClassElement = (Element) element.selectSingleNode("servlet-class");
                String servletClass = servletClassElement.getStringValue();

                // 根据servlet-name的值找到url-pattern
                Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                // /lagou
                String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();
                // 自定义类加载器加载类
                LagouClassLoader classLoader = new LagouClassLoader();
                Class<?> aClass = classLoader.findClass(webXmlPath.replace("web.xml", "") + "/", servletClass);
                Wrapper wrapper = new Wrapper();
                wrapper.setServlet((HttpServlet) aClass.newInstance());
                context.addWrapper(urlPattern, wrapper);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return context;

    }

    /**
     * Minicat 的程序启动入口
     *
     * @param args
     */
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            // 启动Minicat
            bootstrap.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
