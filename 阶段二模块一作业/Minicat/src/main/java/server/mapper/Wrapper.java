package server.mapper;

import server.Request;
import server.Response;
import server.Servlet;

public class Wrapper {

    private Servlet servlet;

    public void setServlet(Servlet servlet) {
        this.servlet = servlet;
    }

    public void init() throws Exception {
        servlet.init();
    }

    public void destroy() throws Exception {
        servlet.destory();
    }

    public void service(Request request, Response response) throws Exception {
        servlet.service(request, response);
    }

}
