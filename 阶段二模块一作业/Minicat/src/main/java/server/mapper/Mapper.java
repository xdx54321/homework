package server.mapper;

import java.util.HashMap;
import java.util.Map;

public class Mapper {

    private Map<String, Host> hostMap = new HashMap<>();

    public Host get(String servletPath) {
        return hostMap.get(servletPath);
    }

    public void add(String servletPath, Host host) {
        hostMap.put(servletPath, host);
    }

}
