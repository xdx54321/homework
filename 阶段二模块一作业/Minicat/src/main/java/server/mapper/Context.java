package server.mapper;

import java.util.HashMap;
import java.util.Map;

public class Context {

    private volatile Map<String, Wrapper> wrapperMap;

    public void addWrapper(String path, Wrapper wrapper) {
        if (wrapperMap == null) {
            wrapperMap = new HashMap<>();
        }
        wrapperMap.put(path, wrapper);
    }

    public Wrapper getWrapper(String path) {
        return wrapperMap.get(path);
    }

}
