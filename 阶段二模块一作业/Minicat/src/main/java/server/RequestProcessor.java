package server;

import server.mapper.Context;
import server.mapper.Mapper;
import server.mapper.Wrapper;

import java.io.File;
import java.io.InputStream;
import java.net.Socket;

public class RequestProcessor extends Thread {

    private Socket socket;

    private Mapper servletMapper;

    public RequestProcessor(Socket socket, Mapper servletMapper) {
        this.socket = socket;
        this.servletMapper = servletMapper;
    }

    @Override
    public void run() {
        try{
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());

            // 静态资源处理
            String projectName = request.getUrl().split("/")[1];
            Context context = servletMapper.get(projectName).getContext();
            Wrapper wrapper = context.getWrapper(request.getUrl().replaceFirst("/" + projectName, ""));
            if(wrapper == null) {
                response.outputHtml(new File("").getCanonicalPath() + "/webapps/" + request.getUrl());
            }else{
                // 动态资源servlet请求
                wrapper.service(request, response);
            }

            socket.close();

        }catch (Exception e) {
            e.printStackTrace();
        }

    }

}
